FROM node:12 as build
WORKDIR /torneo
RUN npm install -g @angular/cli
COPY ./package.json ./package.json
RUN npm i

COPY . .

RUN npm run build:prod
RUN mkdir generado
RUN cp -r dist/* generado/

FROM nginx:1.13.12-alpine
COPY --from=build /torneo/generado /usr/share/nginx/html
COPY /configs/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]

