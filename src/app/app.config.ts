export class AppConfig {
  /* public readonly apiUrl = 'http://jigcaffaratti-001-site1.ftempurl.com/api/';
     public readonly imgUrl = 'http://jigcaffaratti-001-site1.ftempurl.com';
      public readonly assetsUrl = 'http://www.torneogolden.com.ar/assets'*/
  public readonly apiUrl = 'http://localhost:3000/';
  public readonly imgUrl = 'http://localhost:3000';
  public readonly assetsUrl = '/assets';
}
