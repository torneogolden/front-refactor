import { Injectable, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { Usuario } from '../entities/index';

@Injectable()
export class SharedService {
  public newUserSubject = new Subject<any>();
  private user: Usuario = new Usuario();
  navchange: EventEmitter<number> = new EventEmitter();
  phase: EventEmitter<number> = new EventEmitter();

  getUser() {
    return this.user;
  }
  setUser(usuario: Usuario) {
    this.user = usuario;
  }

  addUser(data) {
    this.newUserSubject.next(data);
  }

  emitNavChangeEvent(number) {
    this.navchange.emit(number);
  }
  getNavChangeEmitter() {
    return this.navchange;
  }
  emitPhaseChangeEvent(number) {
    this.phase.emit(number);
  }
  getPhaseChangeEmitter() {
    return this.phase;
  }
}
