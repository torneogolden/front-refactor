import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../../app.config';
import { Categoria, ParametrosFixture } from '../../entities/index';

@Injectable()
export class FixtureService {
  constructor(private http: Http, private config: AppConfig) {}

  create(obj: any, id_zona: number, idTorneo: number, fecha: Date) {
    return this.http.post(
      this.config.apiUrl +
        'fecha/registrar/' +
        id_zona +
        '/' +
        idTorneo +
        '/' +
        fecha,
      obj
    );
  }

  createInterzonal(obj: any, idTorneo: number, idFase: number) {
    return this.http.post(
      this.config.apiUrl +
        'fecha/registrarInterzonal/' +
        idTorneo +
        '/' +
        idFase,
      obj
    );
  }

  update(obj: any, id_zona: number, idTorneo: number) {
    return this.http.post(
      this.config.apiUrl + 'fecha/modificar/' + id_zona + '/' + idTorneo,
      obj
    );
  }

  verificarFecha(obj: any, id_zona: number, idTorneo: number) {
    return this.http.post(
      this.config.apiUrl + 'fecha/verificar/' + id_zona + '/' + idTorneo,
      obj
    );
  }

  obtenerPartidos(
    obj: any,
    idTorneo: number,
    id_zona?: number,
    esInterzonal?: number
  ) {
    if (!esInterzonal) {
      return this.http
        .post(
          this.config.apiUrl + 'fecha/obtener/' + idTorneo + '/' + id_zona,
          obj
        )
        .map((response: Response) => response.json());
    } else {
      return this.http
        .post(
          this.config.apiUrl +
            'fecha/obtener/' +
            idTorneo +
            '/' +
            id_zona +
            '/' +
            esInterzonal,
          obj
        )
        .map((response: Response) => response.json());
    }
  }
  obtenerPartidosClub(obj: any) {
    return this.http
      .post(this.config.apiUrl + 'fecha/obtenerPartidos', obj)
      .map((response: Response) => response.json());
  }
  eliminarPartido(obj: any, idFase: number) {
    return this.http
      .post(this.config.apiUrl + 'fecha/eliminarPartido/' + idFase, obj)
      .map((response: Response) => response.json());
  }

  obtenerFechas(id_zona: number, idTorneo: number) {
    return this.http
      .get(
        this.config.apiUrl + 'fecha/obtenerTodas/' + id_zona + '/' + idTorneo
      )
      .map((response: Response) => response.json());
  }
  obtenerFechasParaSanciones(idTorneo: number) {
    return this.http
      .get(this.config.apiUrl + 'fecha/obtenerTodasParaSancion/' + idTorneo)
      .map((response: Response) => response.json());
  }

  obtenerFechasInterzonales(idTorneo: number) {
    return this.http
      .get(this.config.apiUrl + 'fixture/fecha/obtener/' + idTorneo)
      .map((response: Response) => response.json());
  }

  modificarFecha(obj: any) {
    return this.http
      .post(this.config.apiUrl + 'fecha/modificarFecha', obj)
      .map((response: Response) => response.json());
  }

  modificarFechaInterzonal(idTorneo: number, nuevaFecha: any, fechaVieja: any) {
    return this.http
      .post(
        this.config.apiUrl +
          'fecha/modificarFechaInterzonal/' +
          idTorneo +
          '/' +
          nuevaFecha,
        fechaVieja
      )
      .map((response: Response) => response.json());
  }

  obtenerPartidoFZEquipo(
    obj: any,
    idEquipo: number,
    idTorneo: number,
    id_zona?: number,
    esInterzonal?: number
  ) {
    if (!esInterzonal) {
      return this.http
        .post(
          this.config.apiUrl +
            'fecha/obtenerPartido/' +
            idEquipo +
            '/' +
            idTorneo +
            '/' +
            id_zona,
          obj
        )
        .map((response: Response) => response.json());
    } else {
      return this.http
        .post(
          this.config.apiUrl +
            'fecha/obtenerPartido/' +
            idEquipo +
            '/' +
            idTorneo +
            '/' +
            id_zona +
            '/' +
            esInterzonal,
          obj
        )
        .map((response: Response) => response.json());
    }
  }

  obtenerFixtureFecha(obj: any, idTorneo: number) {
    return this.http
      .post(this.config.apiUrl + 'fixture/fecha/partidos/' + idTorneo, obj)
      .map((response: Response) => response.json());
  }

  obtenerResultadosFecha(obj: any, idTorneo: number) {
    return this.http
      .post(
        this.config.apiUrl + 'fixture/fecha/disputados/' + idTorneo,
        obj
      )
      .map((response: Response) => response.json());
  }

  obtenerFechasJugadas(idTorneo: number) {
    return this.http
      .get(this.config.apiUrl + 'fixture/fecha/disputadas/' + idTorneo)
      .map((response: Response) => response.json());
  }

  generarFixtureAutomatico(parametros: ParametrosFixture) {
    return this.http
      .post(this.config.apiUrl + 'fixtureAutomatico/torneo', parametros)
      .map((response: Response) => response.json());
  }

  obtenerTiposDeFixture() {
    return this.http
      .get(this.config.apiUrl + 'fixtureAutomatico/obtenerTipos/')
      .map((response: Response) => response.json());
  }
}
