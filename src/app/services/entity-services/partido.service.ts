import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { AppConfig } from '../../app.config';
import { Partido } from '../../entities/index';

@Injectable()
export class PartidoService {
    constructor(private http: Http, private config: AppConfig) { }

    create(partidos: Array<Partido>, idFase: number, idTorneo: number
        , esInterzonal: number) {
        return this.http.post(this.config.apiUrl + 'partidos/registrar/' + idFase + '/'
            + idTorneo + '/' + esInterzonal, partidos);
    }

    update(partido: Partido, idFase: number, idTorneo: number
        , esInterzonal: number) {
        return this.http.post(this.config.apiUrl + 'partidos/modificar/' + idFase + '/'
            + idTorneo + '/' + esInterzonal, partido);
    }

    borrarSancion(id_sancion: number) {
        return this.http.get(this.config.apiUrl + 'partido/eliminar/sancion/' + id_sancion).map((response: Response) => response.json());
    }

    borrarGol(id_gol: number, idFase: number, id_zona: number) {
        return this.http.get(this.config.apiUrl + 'partido/eliminar/gol/' + id_gol
            + '/' + idFase + '/' + id_zona).map((response: Response) => response.json());
    }
}
