import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AppConfig } from '../../app.config';

@Injectable()
export class PosicionesService {
  constructor(private http: Http, private config: AppConfig) {}

  getPosiciones(id: number) {
    return this.http
      .get(this.config.apiUrl + 'posicion/general/' + id)
      .map((response: Response) => response.json());
  }

  getPosicionesZona(id: number) {
    return this.http
      .get(this.config.apiUrl + `posicion/zonas/${id}`)
      .map((response: Response) => response.json());
  }

  getGoleadoresPorTorneo(id: number) {
    return this.http
      .get(`${this.config.apiUrl}torneo/goleadores/${id}`)
      .map((response: Response) => response.json());
  }
}
