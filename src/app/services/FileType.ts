export enum FileType {
  ESCUDO = 1,
  CAMISETA = 2,
  CAMISETA_ESCUDO = 3,
  NOTICIA = 4,
}
