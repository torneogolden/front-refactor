import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { IEquipo, IPartido, Equipo, Partido, Fecha, Gol, Sancion, Resultado, ResultadoZona } from '../../entities/index';

@Injectable()
export class ParserService {
    constructor() { }

    parsePartidos(partidos: Array<IPartido>, fecha: Fecha) {
        const lsPartidos = new Array<Partido>();
        for (let i = 0; i < partidos.length; i++) {
            const partido = new Partido();
            const local = new Equipo();
            const visitante = new Equipo();

            partido.horario = partidos[i].horario;
            partido.cancha = partidos[i].cancha;
            partido.fecha.fecha = fecha.fecha;
            if (partidos[i].etapa) {
                partido.etapa = partidos[i].etapa;
            }
            if (partidos[i].llave) {
                partido.llave = partidos[i].llave;
            }
            partido.fecha.id_fecha = fecha.id_fecha;
            partido.estado.idEstado = 1;
            if (partidos[i].id_partido > 0) {
                partido.idPartido = partidos[i].id_partido;
            }

            for (let j = 0; j < partidos[i].local.length; j++) {
                local.idEquipo = partidos[i].local[j].idEquipo;
                local.nombre = partidos[i].local[j].nombre;
                partido.local = local;
            }

            for (let f = 0; f < partidos[i].visitante.length; f++) {
                visitante.idEquipo = partidos[i].visitante[f].idEquipo;
                visitante.nombre = partidos[i].visitante[f].nombre;
                partido.visitante = visitante;
            }
            lsPartidos.push(partido);
        }
        return lsPartidos;
    }

    parseInterezonales(partidos: Array<IPartido>, fecha: Fecha) {
        const lsPartidos = new Array<Partido>();
        for (let i = 0; i < partidos.length; i++) {
            const partido = new Partido();
            const local = new Equipo();
            const visitante = new Equipo();

            partido.horario = partidos[i].horario;
            partido.cancha = partidos[i].cancha;
            partido.fecha.fecha = fecha.fecha;
            partido.fecha.id_fecha = fecha.id_fecha;
            partido.estado.idEstado = 1;
            if (partidos[i].id_partido > 0) {
                partido.idPartido = partidos[i].id_partido;
            }

            for (let j = 0; j < partidos[i].local.length; j++) {
                local.idEquipo = partidos[i].local[j].idEquipo;
                local.nombre = partidos[i].local[j].nombre;
                partido.local = local;
            }

            for (let f = 0; f < partidos[i].visitante.length; f++) {
                visitante.idEquipo = partidos[i].visitante[f].idEquipo;
                visitante.nombre = partidos[i].visitante[f].nombre;
                partido.visitante = visitante;
            }
            lsPartidos.push(partido);
        }
        return lsPartidos;
    }

    parseResultados(partidos: Array<IPartido>, idTorneo: number) {
        const lsPartidos = new Array<Partido>();
        for (let i = 0; i < partidos.length; i++) {
            const partido = new Partido();
            const local = new Equipo();
            const visitante = new Equipo();

            partido.estado.idEstado = 1;
            if (partidos[i].id_partido > 0) {
                partido.idPartido = partidos[i].id_partido;
            }

            if (partidos[i].golesLocal) {
                partido.golesLocal = partidos[i].golesLocal;
            } else {
                partido.golesLocal = new Array<Gol>();
            }

            if (partidos[i].golesVisitante) {
                partido.golesVisitante = partidos[i].golesVisitante;
            } else {
                partido.golesVisitante = new Array<Gol>();
            }
            //Checkeos para playoff
            if (partidos[i].golesLocal.length == partidos[i].golesVisitante.length) {
                if (partidos[i].ganadorPlayoff != null) {
                    partido.ganador.idEquipo = partidos[i].ganadorPlayoff.idEquipo;
                }
            }

            if (partidos[i].penales) {
                partido.penales = partidos[i].penales;
                partido.detallePenales = partidos[i].detallePenales;
            }
            //Sanciones

            if (partidos[i].sancionesLocal) {
                partido.sanciones = partidos[i].sancionesLocal;
            } else {
                partido.sanciones = new Array<Sancion>();
            }

            if (partidos[i].sancionesVisitante) {
                partido.sanciones = partidos[i].sancionesVisitante;
            } else {
                partido.sanciones = new Array<Sancion>();
            }

            for (let j = 0; j < partidos[i].local.length; j++) {
                local.idEquipo = partidos[i].local[j].idEquipo;
                local.nombre = partidos[i].local[j].nombre;
                partido.local = local;
                partido.local.torneo.idTorneo = idTorneo;
            }

            for (let f = 0; f < partidos[i].visitante.length; f++) {
                visitante.idEquipo = partidos[i].visitante[f].idEquipo;
                visitante.nombre = partidos[i].visitante[f].nombre;
                partido.visitante = visitante;
                partido.visitante.torneo.idTorneo = idTorneo;
            }
            partido.llave = partidos[i].llave;
            partido.etapa = partidos[i].etapa;
            partido.fecha.id_fecha = partidos[i].fecha.id_fecha;
            partido.fecha.fecha = partidos[i].fecha.fecha;
            lsPartidos.push(partido);
        }
        return lsPartidos;
    }

    parseResultado(partidoI: Partido, idTorneo: number) {
        const partido = new Partido();

        const local = new Equipo();
        const visitante = new Equipo();

        partido.fecha = partidoI.fecha;
        partido.estado.idEstado = 1;
        if (partidoI.idPartido > 0) {
            partido.idPartido = partidoI.idPartido;
        }

        if (partidoI.resultado != null && partidoI.resultado.id_resultado != null) {
            partido.resultado.id_resultado = partidoI.resultado.id_resultado;
        }

        if (partidoI.resultadoZona != null && partidoI.resultadoZona.id_resultado != null) {
            partido.resultadoZona.id_resultado = partidoI.resultadoZona.id_resultado;
        }

        if (partidoI.golesLocal) {
            partido.golesLocal = partidoI.golesLocal;
        } else {
            partido.golesLocal = new Array<Gol>();
        }

        if (partidoI.golesVisitante) {
            partido.golesVisitante = partidoI.golesVisitante;
        } else {
            partido.golesVisitante = new Array<Gol>();
        }

        if (partidoI.golesABorrar) {
            partido.golesABorrar = partidoI.golesABorrar;
        } else {
            partido.golesABorrar = new Array<Gol>();
        }
        //Sanciones

        if (partidoI.sanciones) {
            partido.sanciones = partidoI.sanciones;
        } else {
            partido.sanciones = new Array<Sancion>();
        }

        if (partidoI.sanciones) {
            partido.sanciones = partidoI.sanciones;
        } else {
            partido.sanciones = new Array<Sancion>();
        }

       /* for (let j = 0; j < partidoI.local.length; j++) {
            local.idEquipo = partidoI.local[j].idEquipo;
            local.nombre = partidoI.local[j].nombre;
            partido.local = local;
            partido.local.torneo.idTorneo = idTorneo;
        }

        for (let f = 0; f < partidoI.visitante.length; f++) {
            visitante.idEquipo = partidoI.visitante[f].idEquipo;
            visitante.nombre = partidoI.visitante[f].nombre;
            partido.visitante = visitante;
            partido.visitante.torneo.idTorneo = idTorneo;
        }*/
        partido.llave = partidoI.llave;

        return partido;
    }
}
