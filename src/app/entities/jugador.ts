import { Equipo, Gol, Persona, Sancion } from './index';

export class Jugador extends Persona {
  public idJugador: number;
  public numero: number;
  public rol: string;
  public fechaAlta: Date;
  public equipo: Equipo;
  public acumAmarillas: number;
  public acumRojas: number;
  public tieneUltSancion: boolean;
  public goles: Gol[];
  public sanciones: Sancion[];
  constructor(
    idJugador?: number,
    numero?: number,
    rol?: string,
    fechaAlta?: Date,
    equipo?: Equipo,
    acumAmarillas?: number,
    acumRojas?: number,
    tieneUltSancion?: boolean,
    goles?: Gol[],
    sanciones?: Sancion[]
  ) {
    super();
    this.idJugador = idJugador;
    this.numero = numero;
    this.rol = rol;
    this.fechaAlta = fechaAlta;
    this.equipo = equipo;
    this.acumAmarillas = acumAmarillas;
    this.acumRojas = acumRojas;
    this.tieneUltSancion = tieneUltSancion;
    this.goles = goles;
    this.sanciones = sanciones;
  }
}
