import { Equipo, Zona, Torneo } from './index';
export class SancionEquipo {
  public idSancionEquipo: number;
  public descripcion: string;
  public puntosRestados: number;
  public equipo: Equipo;
  public zona: Zona;
  public torneo: Torneo;
  public fairPlay: number;

  constructor(
    idSancionEquipo?: number,
    descripcion?: string,
    puntosRestados?: number,
    equipo?: Equipo,
    zona?: Zona,
    torneo?: Torneo,
    fairPlay?: number
  ) {
    this.idSancionEquipo = idSancionEquipo;
    this.descripcion = descripcion;
    this.puntosRestados = puntosRestados;
    this.equipo = equipo;
    this.zona = zona;
    this.torneo = torneo;
    this.fairPlay = fairPlay;
  }
}
