import { IJugador } from './index';
export class IEquipoPlanilla {
  public idEquipo: number;
  public nombre: string;
  public lsJugadores: Array<IJugador>;


  constructor(
    idEquipo?: number,
    nombre?: string,
    lsJugadores?: Array<IJugador>
  ) {

    if (idEquipo) this.idEquipo = idEquipo;
    else this.idEquipo = null;

    if (nombre) this.nombre = nombre;
    else this.nombre = null;

    if (lsJugadores) this.lsJugadores = lsJugadores;
    else this.lsJugadores = new Array<IJugador>();
  }
}
