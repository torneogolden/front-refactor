export class File {
  id: number;
  fileName: string;
  imagePath: string;
  thumbPath: string;
  projectId: string;
  sectionId: string;
  fileSize: number;
  idEquipo: number;
  idTipoArchivo: number;

  constructor(
    id: number,
    fileName: string,
    imagePath: string,
    thumbPath: string,
    projectId: string,
    sectionId: string,
    fileSize: number,
    idEquipo?: number,
    idTipoArchivo?: number
  ) {
    this.id = id;
    this.fileName = fileName;
    this.imagePath = imagePath;
    this.thumbPath = thumbPath;
    this.projectId = projectId;
    this.sectionId = sectionId;
    this.fileSize = fileSize;
    this.idEquipo = idEquipo;
    this.idTipoArchivo = idTipoArchivo;
  }
}
