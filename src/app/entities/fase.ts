export class Fase {

    public idFase: number;
    public descripcion: string;
    constructor(
        idFase?: number,
        descripcion?: string,
    ) {
        if (idFase) this.idFase = idFase;
        else this.idFase = null;

        if (descripcion) this.descripcion = descripcion;
        else this.descripcion = null;

    }
}
