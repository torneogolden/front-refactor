import {
  Partido,
  Jugador,
  TipoSancion,
  Equipo,
  Fecha,
  Zona,
  Fase,
} from './index';

export class Sancion {
  public idSancion: number;
  public fechaInicio: Fecha;
  public fechaFin: Fecha;
  public jugador: Jugador;
  public partido: Partido;
  public equipo: Equipo;
  public detalle: string;
  public tipo: TipoSancion;
  public zona: Zona;
  public fase: Fase;

  constructor(
    idSancion?: number,
    fechaInicio?: Fecha,
    fechaFin?: Fecha,
    jugador?: Jugador,
    partido?: Partido,
    equipo?: Equipo,
    detalle?: string,
    tipo?: TipoSancion,
    zona?: Zona,
    fase?: Fase
  ) {
    this.idSancion = idSancion;
    this.fechaInicio = fechaInicio;
    this.fechaFin = fechaFin;
    this.jugador = jugador;
    this.partido = partido;
    this.equipo = equipo;
    this.detalle = detalle;
    this.tipo = tipo;
    this.zona = zona;
    this.fase = fase;
  }
}
