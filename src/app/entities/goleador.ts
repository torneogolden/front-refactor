import { Torneo, Jugador, Equipo } from './index';

export class Goleador {
  public idGoleador: number;
  public cantidadGoles: number;
  public torneo: Torneo;
  public equipo: Equipo;
  public jugador: Jugador;

  constructor(
    idGoleador: number,
    cantidadGoles: number,
    torneo: Torneo,
    equipo: Equipo,
    jugador: Jugador
  ) {
    this.idGoleador = idGoleador;
    this.cantidadGoles = cantidadGoles;
    this.torneo = torneo;
    this.equipo = equipo;
    this.jugador = jugador;
  }
}
