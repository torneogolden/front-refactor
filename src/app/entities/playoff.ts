import { Llave, Etapa, Partido, Torneo, Equipo } from './index';

export class Playoff {
  public idPlayoff: number;
  public llave: Llave;
  public local: Equipo;
  public visitante: Equipo;
  public ganador: Equipo;
  public etapa: Etapa;
  public torneo: Torneo;
  public partido: Partido;

  constructor(
    idPlayoff: number,
    llave: Llave,
    local: Equipo,
    visitante: Equipo,
    ganador: Equipo,
    etapa: Etapa,
    torneo: Torneo,
    partido: Partido
  ) {
    this.idPlayoff = idPlayoff;
    this.llave = llave;
    this.local = local;
    this.visitante = visitante;
    this.ganador = ganador;
    this.etapa = etapa;
    this.torneo = torneo;
    this.partido = partido;
  }
}
