import {
  Equipo,
  Arbitro,
  Veedor,
  Resultado,
  ResultadoZona,
  HorarioFijo,
  Fecha,
  EstadoPartido,
  Cancha,
  Gol,
  Sancion,
  Llave,
  Etapa,
} from './index';

export class Partido {
  public idPartido: number;
  public duracion: string;
  public fecha: Fecha;
  public inicio: string;
  public fin: string;
  public local: Equipo;
  public visitante: Equipo;
  public arbitro: Arbitro;
  public estado: EstadoPartido;
  public cancha: Cancha;
  public veedor: Veedor;
  public horario: HorarioFijo;
  public resultado: Resultado;
  public resultadoZona: ResultadoZona;
  public goles: Gol[];
  public sanciones: Sancion[];
  public llave: Llave;
  public etapa: Etapa;
  ganador: Equipo;
  penales: boolean;
  detallePenales: string;
  golesABorrar: Gol[];
  golesVisitante: Gol[];
  golesLocal: Gol[];

  constructor(
    idPartido?: number,
    duracion?: string,
    fecha?: Fecha,
    inicio?: string,
    fin?: string,
    local?: Equipo,
    visitante?: Equipo,
    arbitro?: Arbitro,
    estado?: EstadoPartido,
    cancha?: Cancha,
    veedor?: Veedor,
    horario?: HorarioFijo,
    resultado?: Resultado,
    resultadoZona?: ResultadoZona,
    goles?: Gol[],
    sanciones?: Sancion[],
    llave?: Llave,
    etapa?: Etapa,
    ganador?: Equipo,
    penales?: boolean,
    detallePenales?: string,
    golesABorrar?: Gol[],
    golesVisitante?: Gol[],
    golesLocal?: Gol[]
  ) {
    this.idPartido = idPartido;
    this.duracion = duracion;
    this.fecha = fecha;
    this.inicio = inicio;
    this.fin = fin;
    this.local = local;
    this.visitante = visitante;
    this.arbitro = arbitro;
    this.estado = estado;
    this.cancha = cancha;
    this.veedor = veedor;
    this.horario = horario;
    this.resultado = resultado;
    this.resultadoZona = resultadoZona;
    this.goles = goles;
    this.sanciones = sanciones;
    this.llave = llave;
    this.etapa = etapa;
    this.ganador = ganador;
    this.penales = penales;
    this.detallePenales = detallePenales;
    this.golesABorrar = golesABorrar;
    this.golesVisitante = golesVisitante;
    this.golesLocal = golesLocal;
  }
}
