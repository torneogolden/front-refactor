import { Equipo, Torneo, Zona } from './index';

export class PosicionZona {
  public idPosicion: number;
  public equipo: Equipo;
  public puntos: number;
  public golesFavor: number;
  public golesContra: number;
  public difGol: number;
  public torneo: Torneo;
  public zona: Zona;
  public partidosJugados: number;
  public partidosGanados: number;
  public partidosEmpatados: number;
  public partidosPerdidos: number;

  constructor(
    idPosicion: number,
    equipo: Equipo,
    puntos: number,
    golesFavor: number,
    golesContra: number,
    difGol: number,
    torneo: Torneo,
    zona: Zona,
    partidosJugados: number,
    partidosGanados: number,
    partidosEmpatados: number,
    partidosPerdidos: number
  ) {
    this.idPosicion = idPosicion;
    this.equipo = equipo;
    this.puntos = puntos;
    this.golesFavor = golesFavor;
    this.golesContra = golesContra;
    this.difGol = difGol;
    this.torneo = torneo;
    this.zona = zona;
    this.partidosJugados = partidosJugados;
    this.partidosGanados = partidosGanados;
    this.partidosEmpatados = partidosEmpatados;
    this.partidosPerdidos = partidosPerdidos;
  }
}
