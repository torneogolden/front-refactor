import { Torneo, Club, CategoriaNoticia, File } from './index';

export class Noticia {
  public idNoticia: number;
  public titulo: string;
  public descripcion: string;
  public fecha: Date;
  public torneo: Torneo;
  public club: Club;
  public categoriaNoticia: CategoriaNoticia;
  public tags: string;
  public id_thumbnail: number;
  file: File;

  constructor(
    idNoticia?: number,
    titulo?: string,
    descripcion?: string,
    fecha?: Date,
    torneo?: Torneo,
    club?: Club,
    categoriaNoticia?: CategoriaNoticia,
    tags?: string,
    id_thumbnail?: number,
    file?: File
  ) {
    this.file = file;
    if (idNoticia) {
      this.idNoticia = idNoticia;
    } else {
      this.idNoticia = null;
    }

    if (titulo) {
      this.titulo = titulo;
    } else {
      this.titulo = null;
    }

    if (descripcion) {
      this.descripcion = descripcion;
    } else {
      this.descripcion = null;
    }

    if (fecha) {
      this.fecha = fecha;
    } else {
      this.fecha = null;
    }

    if (torneo) {
      this.torneo = torneo;
    } else {
      this.torneo = new Torneo();
    }

    if (club) {
      this.club = club;
    } else {
      this.club = new Club();
    }

    if (categoriaNoticia) {
      this.categoriaNoticia = categoriaNoticia;
    } else {
      this.categoriaNoticia = new CategoriaNoticia();
    }

    if (tags) {
      this.tags = tags;
    } else {
      this.tags = null;
    }

    if (id_thumbnail) {
      this.id_thumbnail = id_thumbnail;
    } else {
      this.id_thumbnail = null;
    }
  }
}
