export class Reglamento {
    public id_reglamento: number;
    public descripcion: string;
    public idTorneo: number;

    constructor(
        id_reglamento?: number,
        descripcion?: string,
        idTorneo?: number
    ) {
        if (id_reglamento) this.id_reglamento = id_reglamento;
        else this.id_reglamento = null;

        if (descripcion) this.descripcion = descripcion;
        else this.descripcion = null;

        if (idTorneo) this.idTorneo = idTorneo;
        else this.idTorneo = null;
    }
}
