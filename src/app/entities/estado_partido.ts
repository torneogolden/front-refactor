export class EstadoPartido {
    public descripcion: string;
    public idEstado: number;
    constructor(
        idEstado?: number,
        descripcion?: string
    ) {
        if (descripcion) this.descripcion = descripcion;
        else this.descripcion = null;

        if (idEstado) this.idEstado = idEstado;
        else this.idEstado = null;
    }
}
