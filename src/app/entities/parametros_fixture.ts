import {
    TipoFixture, Zona
} from './index';

export class ParametrosFixture {
    public zona: Zona;
    public idTorneo: number;
    public idFase: number;
    public cantidadDiasEntrePartidos: number;
    public tipoDeFixture: TipoFixture;
    public esInterzonal: Boolean;
    public intercalarLocalVisitante: Boolean;
    public fechaInicioFixture: Date;

    constructor(
        zona?: Zona,
        idTorneo?: number,
        idFase?: number,
        cantidadDiasEntrePartidos?: number,
        tipoDeFixture?: TipoFixture,
        esInterzonal?: Boolean,
        intercalarLocalVisitante?: Boolean,
        fechaInicioFixture?: Date,
    ) {
        if (zona) this.zona = zona;
        else this.zona = new Zona();

        if (idTorneo) this.idTorneo = idTorneo;
        else this.idTorneo = null;

        if (idFase) this.idFase = idFase;
        else this.idFase = null;

        if (cantidadDiasEntrePartidos) this.cantidadDiasEntrePartidos = cantidadDiasEntrePartidos;
        else this.cantidadDiasEntrePartidos = null;

        if (tipoDeFixture) this.tipoDeFixture = tipoDeFixture;
        else this.tipoDeFixture = new TipoFixture();

        if (esInterzonal) this.esInterzonal = esInterzonal;
        else this.esInterzonal = null;

        if (intercalarLocalVisitante) this.intercalarLocalVisitante = intercalarLocalVisitante;
        else this.intercalarLocalVisitante = null;

        if (fechaInicioFixture) this.fechaInicioFixture = fechaInicioFixture;
        else this.fechaInicioFixture = new Date();
    }
}
