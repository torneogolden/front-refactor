import { Component, OnInit, ViewChild, ViewContainerRef, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ToastsManager, Toast, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { SancionEquipo, Equipo, Torneo } from '../../../entities/index';
import { SancionEquipoService, EquipoService } from '../../../services/entity-services/index';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
    selector: 'sanciones-equipo-carga',
    moduleId: module.id,
    templateUrl: './sanciones-equipo-carga.component.html',
    styleUrls: ['./sanciones-equipo-carga.component.css'],
    providers: []
})
export class SancionEquipoCargaComponent implements OnInit {
    @ViewChild('sancionesEquipoForm') sancionesEquipoForm: FormGroup;
    public sancion = new SancionEquipo();
    public lsEquipos = new Array<Equipo>();


    constructor(
        public toastr: ToastsManager,
        private router: Router,
        private equipoService: EquipoService,
        private sancionEquipoService: SancionEquipoService,
        private spinnerService: Ng4LoadingSpinnerService,
    ) {
    }

    ngOnInit() {
        this.equipoService.getAll().subscribe(
            data => {
                for (let i = 0; i < data.length; i++) {
                    let equipo = new Equipo();
                    equipo = data[i];
                    if (equipo.torneo.idTorneo != null && equipo.torneo.idTorneo == JSON.parse(sessionStorage.getItem('idTorneo'))) {
                        this.lsEquipos.push(equipo);
                    }
                }
            },
            error => {
                this.lsEquipos = new Array<Equipo>();
                error.json()['Message'];
            });

        const idTorneo = Number(sessionStorage.getItem('idTorneo'));
        this.sancion.torneo = new Torneo();
        this.sancion.torneo.idTorneo = idTorneo;
    }

    registrarSancion() {
        if (this.sancion.puntosRestados < 0) {
            this.toastr.error('Los puntos a restar y los puntos de fairplay no pueden ser 0 o negativos', 'Error!');
            return;
        }
        this.spinnerService.show();
        this.sancionEquipoService.create(this.sancion).subscribe(
            data => {
                this.spinnerService.hide();
                this.toastr.success('La sanción se ha registrado con éxito.', 'Exito!');
                this.limpiar();
            },
            error => {
                this.spinnerService.hide();
                this.toastr.error('La sanción no se ha registrado.", "Error!');
            });
    }


    routeAlta() {
        this.router.navigate(['home/configuraciones/sanciones-equipo-carga']);
    }

    routeModificacion() {
         this.router.navigate(['home/configuraciones/sanciones-equipo-baja']);
     }

    limpiar() {
        this.sancion = new SancionEquipo();
        const idTorneo = Number(sessionStorage.getItem('idTorneo'));
        this.sancion.torneo = new Torneo();
        this.sancion.torneo.idTorneo = idTorneo;
    }
}
