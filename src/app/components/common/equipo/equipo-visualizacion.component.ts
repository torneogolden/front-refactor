import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from '../../../app.config';
import { Equipo, Jugador, Usuario } from '../../../entities/index';
import { FileService } from '../../../services/entity-services/file.service';
import { EquipoService } from '../../../services/entity-services/index';
import { FileType } from '../../../services/FileType';
import { ConfirmationDialog } from '../../common/dialog/index';
import * as moment from 'moment';

@Component({
  selector: 'equipo-visualizacion',
  moduleId: module.id,
  templateUrl: './equipo-visualizacion.component.html',
  styleUrls: ['./equipo-visualizacion.component.scss'],
  providers: []
})
export class EquipoVisualizacionComponent implements OnInit {
  idEquipo: number;
  lsJugadores = new Array<Jugador>();
  equipo = new Equipo();
  dt = new Jugador();
  goleador = new Jugador();
  representante = new Jugador();
  nombre_torneo: String;
  escudo: string;
  camiseta: string;
  user: Usuario;
  esAdmin = false;
  dialogRef: MatDialogRef<ConfirmationDialog>;
  private sub: any;
  idTorneo: number;

  constructor(public equipoService: EquipoService, public fileService: FileService, public route: ActivatedRoute, public dialog: MatDialog, public config: AppConfig) {
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idEquipo = +params['id'];

      this.nombre_torneo = sessionStorage.getItem('torneo');

      this.equipoService.getiJugadoresByidEquipo(this.idEquipo, this.idTorneo).subscribe(
        (data) => {
          let max = 0;
          data.forEach((x) => {
            x.goles = x.jugadores.goles && x.jugadores.goles.length ? x.jugadores.goles.filter((y) => y.idTorneo === x.jugadores.equipo.idTorneo) : [];
            x.sanciones = x.jugadores.sanciones && x.jugadores.sanciones.length ? x.jugadores.sanciones.filter((y) => y.idTorneo === x.jugadores.equipo.idTorneo) : [];
            x.rol = x.jugadores.rol;
            const timeDiff = Math.abs(Date.now() - new Date(x.fechaNacimiento).getTime());
            x.edad = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
            const acumAmarillas = x.sanciones && x.sanciones.length ? x.sanciones.filter((y) => y.tipo.idTipo === 1).length : 0;
            x.acumAmarillas = acumAmarillas > 4 ? acumAmarillas - 5 : acumAmarillas;

            const sanciones = x.sanciones && x.sanciones.length ? x.sanciones.filter((y) => y.tipo.idTipo !== 1) : [];
            if (sanciones && sanciones.length) {
              let ultSancion;
              sanciones.forEach((z) => {
                if (moment(z.fechaFinB.fecha).isSameOrAfter(moment(), 'day')) {
                  ultSancion = z;
                }
              });
              x.tieneUltSancion = ultSancion ? ultSancion.tipo.descripcion : false;
            }
            if (x.goles.length >= max) {
              max = x.goles.length;
            }
          });
          this.lsJugadores = data;
          this.goleador = this.lsJugadores.find((x) => x.goles.length === max);
          this.representante = this.lsJugadores.find((y) => y.rol === 'representante');
          this.dt = this.lsJugadores.find((y) => y.rol === 'director_tecnico');
        },
        (error) => {
          error.json()['Message'];
        }
      );

      this.equipoService.getById(this.idEquipo).subscribe(
        (data) => {
          this.equipo = data;
          this.equipo.nombre = this.equipo.nombre.toUpperCase();
          this.camiseta = this.equipo.files.find((x) => x.idTipoArchivo === FileType.CAMISETA).imagePath;
          this.escudo = this.equipo.files.find((x) => x.idTipoArchivo === FileType.ESCUDO).imagePath;
        },
        (error) => {
          error.json()['Message'];
        }
      );

      this.user = JSON.parse(sessionStorage.getItem('currentUser'));
      if (this.user != null) {
        if (this.user.perfil.id_perfil == 1) {
          this.esAdmin = true;
        }
      }
    });
  }

  getFullName(jugador: Jugador) {
    return jugador ? `${jugador.apellido} , ${jugador.nombre}` : '-';
  }
}
