import { Component, Directive, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { EquipoService } from '../../../services/entity-services/index';
import { Equipo, IEquipo } from '../../../entities/index';
import { FileService } from '../../../services/entity-services/file.service';
import { AppConfig } from '../../../app.config';
import { FileType } from '../../../services/FileType';

@Component({
  selector: 'equipos-torneo',
  moduleId: module.id,
  templateUrl: './equipos-torneo.component.html',
  styleUrls: ['./equipos-torneo.component.scss'],
  providers: [],
})
export class EquiposTorneoComponent implements OnInit {
  lsEquipos = new Array<Equipo>();

  constructor(
    public equipoService: EquipoService,
    public fileService: FileService,
    private router: Router,
    public config: AppConfig
  ) {}

  ngOnInit() {
    const idTorneo = Number(sessionStorage.getItem('idTorneo'));

    this.equipoService.getAllPorTorneo(idTorneo).subscribe(
      (data) => {
        this.lsEquipos = data.filter(
          (x) =>
            (x.files = x.files.filter(
              (y) => y.idTipoArchivo === FileType.ESCUDO
            ))
        );
      },
      (error) => {
        error.json()['Message'];
      }
    );
  }

  // METODOS-----------------------------------------------------------------------------

  verEquipo(idEquipo: number) {
    this.router.navigate(['home/equipo/' + idEquipo]);
  }
}
