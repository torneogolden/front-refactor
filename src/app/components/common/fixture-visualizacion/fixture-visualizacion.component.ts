import { Component, Input, OnInit } from '@angular/core';
import { AppConfig } from '../../../app.config';
import { Fecha, Fixture, Partido } from '../../../entities/index';
import { FileService } from '../../../services/entity-services/file.service';
import {
  EquipoService,
  FixtureService
} from '../../../services/entity-services/index';
import { FileType } from '../../../services/FileType';

@Component({
  selector: 'fixture-visualizacion',
  moduleId: module.id,
  templateUrl: './fixture-visualizacion.component.html',
  styleUrls: ['./fixture-visualizacion.component.scss'],
  providers: [],
})
export class FixtureVisualizacionComponent implements OnInit {
  @Input() fixtureAutomatico: Fixture;
  lsFechas = new Array<Fecha>();
  idTorneo: number;
  fixture = new Fixture();
  lsFechasMostrar = new Array<Fecha>();
  lsPartidos: Partido[];
  fecha = new Fecha();
  diaVisual: any;
  numeroFecha: number;
  fechaSeleccionada = false;

  constructor(
    private equipoService: EquipoService,
    private fileService: FileService,
    private fixtureService: FixtureService,
    public config: AppConfig
  ) {}

  ngOnInit() {
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
    this.fixtureService.obtenerFechasInterzonales(this.idTorneo).subscribe(
      (data) => {
        if (data) {
          this.lsFechas = data;
          if (this.lsFechas.length) {
            this.mostrarFixtureFecha(this.lsFechas[this.lsFechas.length - 1]);
          }
        }
      },
      (error) => {
        this.lsFechas = [];
      }
    );
  }

  mostrarFixtureFecha(fecha: Fecha) {
    this.diaVisual = this.formatearFecha(new Date(fecha.fecha));
    this.numeroFecha = this.lsFechas.findIndex(x => x.fecha === fecha.fecha) + 1;
    this.fechaSeleccionada = true;
    this.fixtureService.obtenerFixtureFecha(fecha, this.idTorneo).subscribe(
      (data) => {
        if (data) {
          data.filter((x) => {
            x.local.files = x.local.files.filter(
              (y) => y.idTipoArchivo === FileType.CAMISETA_ESCUDO
            );
            x.visitante.files = x.visitante.files.filter(
              (y) => y.idTipoArchivo === FileType.CAMISETA_ESCUDO
            );
          });
          this.lsPartidos = data;
        }
      },
      (error) => {
        error.json()['Message'];
      }
    );
  }

  formatearFecha(fecha: Date) {
    const semana = new Array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
    const dia = semana[fecha.getUTCDay()];
    const dd = fecha.getUTCDate();
    const mm = fecha.getMonth() + 1;
    return dia + ' ' + dd + '.' + mm;
  }
}
