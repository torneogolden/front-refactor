import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { Posicion, PosicionZona, SancionEquipo } from '../../../entities/index';
import {
  PosicionesService,
  SancionEquipoService,
} from '../../../services/entity-services/index';
import { AppConfig } from '../../../app.config';
import { FileType } from '../../../services/FileType';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'posiciones-general',
  moduleId: module.id,
  templateUrl: './posiciones-general.component.html',
  styleUrls: ['./posiciones-general.component.scss'],
  providers: [],
})
export class PosicionesGeneralComponent implements OnInit {
  public lsPosiciones = new Array<Posicion>();
  public idFase: number;
  public lsPosicionesZona = new Array<Array<PosicionZona>>();
  public lsSanciones = new Array<SancionEquipo>();

  constructor(
    private posicionesService: PosicionesService,
    private sancionEquipoService: SancionEquipoService,
    public config: AppConfig
  ) {}

  ngOnInit() {
    const idTorneo = Number(sessionStorage.getItem('idTorneo'));
    this.idFase = Number(sessionStorage.getItem('fase'));

    if (this.idFase === 1) {
      this.posicionesService.getPosiciones(idTorneo).subscribe((data) => {
        data.filter((x) => {
          x.equipo.files = x.equipo.files.filter(
            (y) => y.idTipoArchivo === FileType.ESCUDO
          );
        });
        this.lsPosiciones = data;
        this.getSancionesEquipo(this.lsPosiciones);
      });
    } else {
      this.posicionesService.getPosicionesZona(idTorneo).subscribe((data) => {
        data.filter((x) => {
          x.filter((y) => {
            y.equipo.files = y.equipo.files.filter(
              (z) => z.idTipoArchivo === FileType.ESCUDO
            );
          });
        });
        this.lsPosicionesZona = data;
        this.getSancionesEquipo(this.lsPosicionesZona);
      });
    }

    /* this.posicionesService.getPosicionesPorTorneo(idTorneo).subscribe(
      (data) => {
        if (this.idFase == 1) {
          for (let i = 0; i < data.length; i++) {
            let posicion = new Posiciones();
            posicion = data[i];
            this.lsPosiciones.push(posicion);
          }
          this.buscarSancionesGeneral();
        } else {
          for (let i = 0; i < data.length; i++) {
            var lsPosicionZona = new Array<PosicionesZona>();
            lsPosicionZona = data[i];
            this.lsPosicionesZona.push(lsPosicionZona);
          }

          this.buscarSancionesEquiposZona(lsPosicionZona);
        }
      },
      (error) => {
        error.json()["Message"];
      }
    );*/
  }

  public getSancionesEquipo(posiciones: any[]) {
    let comando = posiciones.map(
      (x) =>
        (x = { idTorneo: x.idTorneo, idZona: x.idZona, idEquipo: x.idEquipo })
    );
    if (this.idFase !== 1) {
      comando = [];
      for (const posicionZona of posiciones) {
        comando.push(
          ...posicionZona.map(
            (x) =>
              (x = {
                idTorneo: x.idTorneo,
                idZona: x.idZona,
                idEquipo: x.idEquipo,
              })
          )
        );
      }
    }
    this.sancionEquipoService
      .getSancionesByEquipos(comando, this.idFase)
      .subscribe((data) => {
        this.lsSanciones = data;
      });
  }

  /* buscarSancionesGeneral() {
    for (let i = 0; i < this.lsPosiciones.length; i++) {
      this.sancionEquipoService
        .getSancionesByEquipo(this.lsPosiciones[i].equipo.idEquipo)
        .subscribe(
          (data) => {
            if (data) {
              for (let j = 0; j < data.length; j++) {
                let sancion = new SancionEquipo();
                sancion = data[j];
                this.lsSanciones.push(sancion);
              }
            }
          },
          (error) => {
            error.json()["Message"];
          }
        );
    }
  }

  buscarSancionesEquiposZona(lista: Array<PosicionZona>) {
    if (lista) {
      for (let i = 0; i < lista.length; i++) {
        this.sancionEquipoService
          .getSancionesByEquipo(lista[i].equipo.idEquipo)
          .subscribe(
            (data) => {
              if (data) {
                for (let j = 0; j < data.length; j++) {
                  let sancion = new SancionEquipo();
                  sancion = data[j];
                  this.lsSanciones.push(sancion);
                }
              }
            },
            (error) => {
              error.json()["Message"];
            }
          );
      }
    }
  }*/
}
