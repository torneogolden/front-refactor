import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { Goleador } from '../../../entities/index';
import { PosicionesService } from '../../../services/entity-services/index';
import { AppConfig } from '../../../app.config';
import { FileType } from '../../../services/FileType';

@Component({
  selector: 'goleadores',
  moduleId: module.id,
  templateUrl: './goleadores.component.html',
  styleUrls: ['./goleadores.component.scss'],
  providers: [],
})
export class GoleadoresComponent implements OnInit {
  public lsGoleadores = new Array<Goleador>();

  constructor(
    private posicionesService: PosicionesService,
    public config: AppConfig
  ) {}

  ngOnInit() {
    const idTorneo = Number(sessionStorage.getItem('idTorneo'));

    this.posicionesService.getGoleadoresPorTorneo(idTorneo).subscribe(
      (data) => {
        data.filter((x) => {
          x.equipo.files = x.equipo.files.filter(
            (y) => y.idTipoArchivo === FileType.ESCUDO
          );
          x.jugador = { ...x.jugador.persona };
        });
        this.lsGoleadores = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
