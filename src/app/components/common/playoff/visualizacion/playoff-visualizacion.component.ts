import { Component, OnInit } from '@angular/core';
import { AppConfig } from '../../../../app.config';
import { Playoff, Zona } from '../../../../entities/index';
import {
  PlayoffService,
  ZonaService,
} from '../../../../services/entity-services/index';
import { FileType } from '../../../../services/FileType';

@Component({
  selector: 'playoff-visualizacion',
  moduleId: module.id,
  templateUrl: './playoff-visualizacion.component.html',
  styleUrls: ['./playoff-visualizacion.component.scss'],
  providers: [ZonaService],
})
export class PlayoffVisualizacionComponent implements OnInit {
  idTorneo: number;
  zonas = new Array<Zona>();
  lsPartidos = new Array<Playoff>();

  playoffA: any[] = [];
  cuartosA: any[] = [];
  semisA: any[] = [];
  tercerYCuartoA: any[] = [];
  finalA: any[] = [];

  playoffB: any[] = [];
  cuartosB: any[] = [];
  semisB: any[] = [];
  tercerYCuartoB: any[] = [];
  finalB: any[] = [];

  playoffC: any[] = [];
  cuartosC: any[] = [];
  semisC: any[] = [];
  tercerYCuartoC: any[] = [];
  finalC: any[] = [];

  playoffD: any[] = [];
  cuartosD: any[] = [];
  semisD: any[] = [];
  tercerYCuartoD: any[] = [];
  finalD: any[] = [];

  playoffE: any[] = [];
  cuartosE: any[] = [];
  semisE: any[] = [];
  tercerYCuartoE: any[] = [];
  finalE: any[] = [];

  playoffF: any[] = [];
  cuartosF: any[] = [];
  semisF: any[] = [];
  tercerYCuartoF: any[] = [];
  finalF: any[] = [];

  playoffG: any[] = [];
  cuartosG: any[] = [];
  semisG: any[] = [];
  tercerYCuartoG: any[] = [];
  finalG: any[] = [];

  playoffH: any[] = [];
  cuartosH: any[] = [];
  semisH: any[] = [];
  tercerYCuartoH: any[] = [];
  finalH: any[] = [];

  //Esto se usa para visualizacion mobile.
  playoffActivo: any[] = [];
  cuartosActivo: any[] = [];
  semisActivo: any[] = [];
  tercerYCuartoActivo: any[] = [];
  finalActivo: any[] = [];

  constructor(
    public config: AppConfig,
    public zonaService: ZonaService,
    public playoffService: PlayoffService
  ) {
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
  }

  ngOnInit() {
    this.zonaService.getAll(this.idTorneo).subscribe(
      (data) => {
        data.filter((x) =>
          x.equipos.forEach((y) => {
            y.files = y.files.filter(
              (z) => z.idTipoArchivo === FileType.ESCUDO
            );
          })
        );
        this.zonas = data;
        this.obtenerPartidos();
      },
      (error) => {}
    );
  }

  obtenerPartidos() {
    this.playoffService.getPlayoffsTorneo(this.idTorneo).subscribe(
      (data) => {
        if (data) {
          data.filter((x) => {
            x.golesLocal =
              x.partido.goles && x.partido.goles.length
                ? x.partido.goles.filter((y) => y.idEquipo === x.local.idEquipo)
                : [];
            x.golesVisitante =
              x.partido.goles && x.partido.goles.length
                ? x.partido.goles.filter(
                    (y) => y.idEquipo === x.visitante.idEquipo
                  )
                : [];
            x.local.files = x.local.files.filter(
              (y) => y.idTipoArchivo === FileType.ESCUDO
            );
            x.ganador.files = x.ganador.files.filter(
              (y) => y.idTipoArchivo === FileType.ESCUDO
            );
            x.visitante.files = x.visitante.files.filter(
              (y) => y.idTipoArchivo === FileType.ESCUDO
            );
          });
          this.lsPartidos = data;
        }
        this.armarPlayoff();
      },
      (error) => {}
    );
  }

  armarPlayoff() {
    for (let i = 0; i < this.zonas.length; i++) {
      switch (this.zonas[i].descripcion) {
        case 'Playoff A':
          this.servicioPlayoffA(i);
          break;
        case 'Playoff B':
          this.servicioPlayoffB(i);
          break;
        case 'Playoff C':
          this.servicioPlayoffC(i);
          break;
        case 'Playoff D':
          this.servicioPlayoffD(i);
          break;
        case 'Playoff E':
          this.servicioPlayoffE(i);
          break;
        case 'Playoff F':
          this.servicioPlayoffF(i);
          break;
        case 'Playoff G':
          this.servicioPlayoffG(i);
          break;
        case 'Playoff H':
          this.servicioPlayoffH(i);
          break;
      }
    }
  }

  servicioPlayoffA(i: number) {
    this.playoffA = [];
    this.cuartosA = [];
    this.semisA = [];
    this.tercerYCuartoA = [];
    this.finalA = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosA.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosA.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisA.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisA.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoA.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoA.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalA.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalA.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffA.push(this.cuartosA);
    this.playoffA.push(this.semisA);
    this.playoffA.push(this.tercerYCuartoA);
    this.playoffA.push(this.finalA);
    this.playoffActivo = this.playoffA;
  }

  servicioPlayoffB(i: number) {
    this.playoffB = [];
    this.cuartosB = [];
    this.semisB = [];
    this.tercerYCuartoB = [];
    this.finalB = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosB.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosB.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisB.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisB.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoB.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoB.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalB.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalB.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffB.push(this.cuartosB);
    this.playoffB.push(this.semisB);
    this.playoffB.push(this.tercerYCuartoB);
    this.playoffB.push(this.finalB);
  }

  servicioPlayoffC(i: number) {
    this.playoffC = [];
    this.cuartosC = [];
    this.semisC = [];
    this.tercerYCuartoC = [];
    this.finalC = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosC.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosC.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisC.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisC.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoC.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoC.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalC.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalC.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffC.push(this.cuartosC);
    this.playoffC.push(this.semisC);
    this.playoffC.push(this.tercerYCuartoC);
    this.playoffC.push(this.finalC);
  }

  servicioPlayoffD(i: number) {
    this.playoffD = [];
    this.cuartosD = [];
    this.semisD = [];
    this.tercerYCuartoD = [];
    this.finalD = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosD.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosD.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisD.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisD.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoD.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoD.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalD.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalD.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffD.push(this.cuartosD);
    this.playoffD.push(this.semisD);
    this.playoffD.push(this.tercerYCuartoD);
    this.playoffD.push(this.finalD);
  }

  servicioPlayoffE(i: number) {
    this.playoffE = [];
    this.cuartosE = [];
    this.semisE = [];
    this.tercerYCuartoE = [];
    this.finalE = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosE.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosE.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisE.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisE.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoE.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoE.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalE.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalE.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffE.push(this.cuartosE);
    this.playoffE.push(this.semisE);
    this.playoffE.push(this.tercerYCuartoE);
    this.playoffE.push(this.finalE);
  }

  servicioPlayoffF(i: number) {
    this.playoffF = [];
    this.cuartosF = [];
    this.semisF = [];
    this.tercerYCuartoF = [];
    this.finalF = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosF.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosF.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisF.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisF.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoF.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoF.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalF.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalF.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffF.push(this.cuartosF);
    this.playoffF.push(this.semisF);
    this.playoffF.push(this.tercerYCuartoF);
    this.playoffF.push(this.finalF);
  }

  servicioPlayoffG(i: number) {
    this.playoffG = [];
    this.cuartosG = [];
    this.semisG = [];
    this.tercerYCuartoG = [];
    this.finalG = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosG.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosG.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisG.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisG.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoG.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoG.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalG.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalG.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffG.push(this.cuartosG);
    this.playoffG.push(this.semisG);
    this.playoffG.push(this.tercerYCuartoG);
    this.playoffG.push(this.finalG);
  }

  servicioPlayoffH(i: number) {
    this.playoffH = [];
    this.cuartosH = [];
    this.semisH = [];
    this.tercerYCuartoH = [];
    this.finalH = [];

    for (let f = this.zonas[i].equipos.length - 1; f >= 0; f--) {
      for (let g = this.lsPartidos.length - 1; g >= 0; g--) {
        if (
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].local.idEquipo ||
          this.zonas[i].equipos[f].idEquipo ==
            this.lsPartidos[g].visitante.idEquipo
        ) {
          switch (this.lsPartidos[g].etapa.descripcion) {
            case '4tos':
              this.cuartosH.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.cuartosH.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Semifinal':
              this.semisH.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.semisH.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
            case 'Tercer puesto':
              this.tercerYCuartoH.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.tercerYCuartoH.sort(
                (a, b) => a.llave.idLlave - b.llave.idLlave
              );
              break;
            case 'Final':
              this.finalH.push(this.lsPartidos[g]);
              this.lsPartidos.splice(g, 1);
              this.finalH.sort((a, b) => a.llave.idLlave - b.llave.idLlave);
              break;
          }
        }
      }
    }
    this.playoffH.push(this.cuartosH);
    this.playoffH.push(this.semisH);
    this.playoffH.push(this.tercerYCuartoH);
    this.playoffH.push(this.finalH);
  }

  //PARTE MOBILE

  mostrarPlayoff(i: string) {
    switch (i) {
      case 'A':
        this.playoffActivo = this.playoffA;
        break;
      case 'B':
        this.playoffActivo = this.playoffB;
        break;
      case 'C':
        this.playoffActivo = this.playoffC;
        break;
      case 'D':
        this.playoffActivo = this.playoffD;
        break;
    }
  }
}
