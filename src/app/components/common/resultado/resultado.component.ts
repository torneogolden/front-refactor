import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
  ViewEncapsulation
} from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import {
  FormGroup,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';
import { FileService } from '../../../services/entity-services/file.service';
import { ParserService } from '../../../services/common-services/index';
import {
  Torneo,
  TipoTorneo,
  Modalidad,
  Regla,
  Categoria,
  Equipo,
  Zona,
  Fixture,
  Fecha,
  Cancha,
  HorarioFijo,
  Turno,
  IEquipo,
  IPartido,
  Partido,
  Jugador,
  Gol,
  ResultadoZona,
  Sancion,
  TipoSancion
} from '../../../entities/index';
import {
  EquipoService,
  ZonaService,
  HorarioService,
  CanchaService,
  FixtureService,
  SancionService,
  PartidoService
} from '../../../services/entity-services/index';
import { ToastsManager, Toast, ToastOptions } from 'ng2-toastr/ng2-toastr';
import * as moment from 'moment';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmationDialog } from '../../common/dialog/index';
import { SancionDialog, SancionDialogV } from './index';
import { AppConfig } from '../../../app.config';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'resultado',
  moduleId: module.id,
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [EquipoService, ZonaService]
})
export class ResultadoComponent implements OnInit {
  dialogRef: MatDialogRef<SancionDialog>;
  dialogRefV: MatDialogRef<SancionDialogV>;
  dialogRefBorrado: MatDialogRef<ConfirmationDialog>;

  fecha = new Fecha();
  zona = new Zona();
  partidos = new Array<IPartido>();
  lsCanchas = new Array<Cancha>();
  horarios = new Array<HorarioFijo>();
  jugadorVisitante = new Jugador();
  jugadorLocal = new Jugador();
  JugadorLocalActual = new Jugador();

  equipos = new Array<IEquipo>();
  lsZonas = new Array<Zona>();
  local = new Array<IEquipo>();
  visitante = new Array<IEquipo>();
  lsEquiposPartido = new Array<IEquipo>();
  cantidadPartidos: number;
  imagesEscudos: Array<any> = [];
  cantidadZonas: number;
  idTorneo: number;
  check: Boolean = false;
  lsJugadoresLocal = new Array<Jugador>();
  lsJugadoresVisitante = new Array<Jugador>();
  fixture = new Fixture();
  lsFechasInicio = new Array<Fecha>();
  lsFechasFin = new Array<Fecha>();
  lsTiposSanciones = new Array<TipoSancion>();
  idFase: number;
  esInterzonal = 0;
  checked: boolean;
  evento: boolean;
  deshabilitarImagenes: boolean;
  constructor(
    private fileService: FileService,
    public equipoService: EquipoService,
    private router: Router,
    public zonaService: ZonaService,
    public toastr: ToastsManager,
    public horarioService: HorarioService,
    public canchaService: CanchaService,
    public parserService: ParserService,
    public fixtureService: FixtureService,
    public dialog: MatDialog,
    public sancionService: SancionService,
    public partidoService: PartidoService,
    public config: AppConfig,
    private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
    this.idFase = Number(sessionStorage.getItem('fase'));
  }
  ngOnInit() {
    this.deshabilitarImagenes = true;
    this.zonaService.getAll(this.idTorneo).subscribe(
      data => {
        this.lsZonas = [];
        for (let i = 0; i < data.length; i++) {
          let zona: Zona;
          zona = data[i];
          if (zona.torneo.idTorneo != null) {
            this.lsZonas.push(zona);
          }
        }
      },
      error => {}
    );

    this.sancionService.getAll().subscribe(
      data => {
        this.lsTiposSanciones = [];
        for (let i = 0; i < data.length; i++) {
          let tipo: TipoSancion;
          tipo = data[i];
          if (tipo.id != 1) {
            this.lsTiposSanciones.push(tipo);
          }
        }
      },
      error => {}
    );

    this.horarioService.getAll().subscribe(
      data => {
        this.horarios = [];
        for (let i = 0; i < data.length; i++) {
          let horario = new HorarioFijo();
          horario = data[i];
          this.horarios.push(horario);
        }
      },
      error => {
        error.json()['Message'];
      }
    );

    this.canchaService.getAll().subscribe(
      data => {
        this.lsCanchas = [];
        for (let i = 0; i < data.length; i++) {
          let cancha = new Cancha();
          cancha = data[i];
          this.lsCanchas.push(cancha);
        }
      },
      error => {
        error.json()['Message'];
      }
    );
  }

  checkValue(event: any) {
    this.evento = event;
    if (event) {
      this.esInterzonal = 1;
      this.zona = null;
    } else {
      this.esInterzonal = 0;
    }
    this.obtenerPartidosImplementacion();
  }

  public obtenerPartidosImplementacion() {
    this.partidos = [];
    this.fixtureService
      .obtenerPartidos(
        this.fecha,
        this.idTorneo,
        this.zona == null ? 0 : this.zona.id_zona,
        this.esInterzonal
      )
      .subscribe(
        data => {
          this.partidos = data;
          for (let i = 0; i < this.partidos.length; i++) {
            if (this.partidos[i].local) {
            //  this.obtenerJugadoresLocal(this.partidos[i]);
              this.partidos[i].golesLocal = new Array<Gol>();
              this.partidos[i].sancionesLocal = new Array<Sancion>();
              this.partidos[i].jugadorLocal = new Jugador();
              this.partidos[i].jugadorLocal.acumAmarillas = 0;
              this.partidos[i].jugadorLocal.acumRojas = 0;
              this.partidos[i].desImagenes = true;
            }
            if (this.partidos[i].visitante) {
            //  this.obtenerJugadoresVisitante(this.partidos[i]);
              this.partidos[i].golesVisitante = new Array<Gol>();
              this.partidos[i].sancionesVisitante = new Array<Sancion>();
              this.partidos[i].jugadorVisitante = new Jugador();
              this.partidos[i].jugadorVisitante.acumAmarillas = 0;
              this.partidos[i].jugadorVisitante.acumRojas = 0;
              this.partidos[i].desImagenesV = true;
            }

            if (this.idFase == 3) {
              this.partidos[i].equiposPartido = new Array<IEquipo>();
              this.partidos[i].equiposPartido.push(this.partidos[i].local[0]);
              this.partidos[i].equiposPartido.push(
                this.partidos[i].visitante[0]
              );
            }
          }
        },
        error => {}
      );
  }

  obtenerJugadoresLocal(partido: Partido) {
    this.equipoService
      .getJugadoresByidEquipo(partido.local[0].idEquipo)
      .subscribe(
        data => {
          this.lsJugadoresLocal = [];
          for (let i = 0; i < data.length; i++) {
            let jugador = new Jugador();
            jugador = data[i];
            this.lsJugadoresLocal.push(jugador);
          }
          partido.local[0].lsJugadores = this.lsJugadoresLocal;
        },
        error => {
          error.json()['Message'];
        }
      );
  }

  obtenerJugadoresVisitante(partido: Partido) {
    this.equipoService
      .getJugadoresByidEquipo(partido.visitante[0].idEquipo)
      .subscribe(
        data => {
          this.lsJugadoresVisitante = [];
          for (let i = 0; i < data.length; i++) {
            let jugador = new Jugador();
            jugador = data[i];
            this.lsJugadoresVisitante.push(jugador);
          }
          partido.visitante[0].lsJugadores = this.lsJugadoresVisitante;
        },
        error => {
          error.json()['Message'];
        }
      );
  }

  onChangeLocal(jugador: Jugador, partido: Partido) {
    if (jugador.apellido != null) {
     // partido.desImagenes = false;
    } else {
     // partido.desImagenes = true;
    }

    this.sancionService
      .getAcumuladoTarjetas(this.idTorneo, jugador.idJugador)
      .subscribe(
        data => {
          let sanciones = new Array<Sancion>();
          sanciones = data;
          this.calcularRojasAmarillasL(jugador, sanciones, partido);
        },
        error => {}
      );
  }
  onChangeVisitante(jugador: Jugador, partido: Partido) {
    if (jugador.apellido != null) {
     // partido.desImagenesV = false;
    } else {
     // partido.desImagenesV = true;
    }

    this.sancionService
      .getAcumuladoTarjetas(this.idTorneo, jugador.idJugador)
      .subscribe(
        data => {
          let sanciones = new Array<Sancion>();
          sanciones = data;
          this.calcularRojasAmarillasV(jugador, sanciones, partido);
        },
        error => {}
      );
  }

  calcularRojasAmarillasL(
    jugador: Jugador,
    sanciones: Array<Sancion>,
    partido: Partido
  ) {
    let amarillas = 0;
    let rojas = 0;
    for (let j = 0; j < this.partidos.length; j++) {
      this.partidos[j].jugadorLocal = new Jugador();
    }
    for (let i = 0; i < sanciones.length; i++) {
      if (sanciones[i].tipo.id == 1) {
        //Acumulo Amarillas
        amarillas = amarillas + 1;
      }
      if (sanciones[i].tipo.id == 2) {
        //Acumulo Rojas - No distingo el resto
        rojas = rojas + 1;
      }
    }

    while (amarillas > 4) {
      amarillas = amarillas - 5;
    }
    jugador.acumAmarillas = amarillas;
    jugador.acumRojas = rojas;
    //partido.jugadorLocal = jugador;
  }

  calcularRojasAmarillasV(
    jugador: Jugador,
    sanciones: Array<Sancion>,
    partido: Partido
  ) {
    let amarillas = 0;
    let rojas = 0;
    for (let i = 0; i < sanciones.length; i++) {
      if (sanciones[i].tipo.id == 1) {
        //Acumulo Amarillas
        amarillas = amarillas + 1;
      }
      if (sanciones[i].tipo.id == 2) {
        //Acumulo Rojas - No distingo el resto
        rojas = rojas + 1;
      }
    }
    jugador.acumAmarillas = amarillas;
    jugador.acumRojas = rojas;
    //partido.jugadorVisitante = jugador;
  }

  golLocal(partido: Partido) {
    const gol = new Gol();
    gol.equipo.idEquipo = partido.local[0].idEquipo;
    gol.jugador = this.jugadorLocal;
    gol.partido.idPartido = partido.idPartido;
    partido.golesLocal.push(gol);
  }

  golVisitante(partido: Partido) {
    const gol = new Gol();
    gol.equipo.idEquipo = partido.visitante[0].idEquipo;
    gol.jugador = this.jugadorVisitante;
    gol.partido.idPartido = partido.idPartido;
    // this.golesVisitante.push(gol);
    partido.golesVisitante.push(gol);
  }

  registrarResultado() {
    this.spinnerService.show();
    let lsPartidos = new Array<Partido>();
    lsPartidos = this.parserService.parseResultados(
      this.partidos,
      this.idTorneo
    );

    for (let i = 0; i < lsPartidos.length; i++) {
      lsPartidos[i].local.fair_play = this.calcularFairPlay(
        lsPartidos[i].sanciones
      );
      lsPartidos[i].visitante.fair_play = this.calcularFairPlay(
        lsPartidos[i].sanciones
      );

      if (this.esInterzonal == 1) {
        if (
          lsPartidos[i].golesLocal.length ==
          lsPartidos[i].golesVisitante.length
        ) {
          lsPartidos[i].resultado.ganador.idEquipo =
            lsPartidos[i].local.idEquipo;
          lsPartidos[i].resultado.perdedor.idEquipo =
            lsPartidos[i].visitante.idEquipo;
          lsPartidos[i].resultado.empate = 1;
        } else if (
          lsPartidos[i].golesLocal.length >
          lsPartidos[i].golesVisitante.length
        ) {
          lsPartidos[i].resultado.ganador.idEquipo =
            lsPartidos[i].local.idEquipo;
          lsPartidos[i].resultado.perdedor.idEquipo =
            lsPartidos[i].visitante.idEquipo;
        } else {
          lsPartidos[i].resultado.ganador.idEquipo =
            lsPartidos[i].visitante.idEquipo;
          lsPartidos[i].resultado.perdedor.idEquipo =
            lsPartidos[i].local.idEquipo;
        }
      } else {
        lsPartidos[i].resultadoZona.zona = this.zona;
        if (
          lsPartidos[i].golesLocal.length ==
          lsPartidos[i].golesVisitante.length
        ) {
          lsPartidos[i].resultadoZona.ganador.idEquipo =
            lsPartidos[i].local.idEquipo;
          lsPartidos[i].resultadoZona.perdedor.idEquipo =
            lsPartidos[i].visitante.idEquipo;
          lsPartidos[i].resultadoZona.empate = 1;
        } else if (
          lsPartidos[i].golesLocal.length >
          lsPartidos[i].golesVisitante.length
        ) {
          lsPartidos[i].resultadoZona.ganador.idEquipo =
            lsPartidos[i].local.idEquipo;
          lsPartidos[i].resultadoZona.perdedor.idEquipo =
            lsPartidos[i].visitante.idEquipo;
        } else {
          lsPartidos[i].resultadoZona.ganador.idEquipo =
            lsPartidos[i].visitante.idEquipo;
          lsPartidos[i].resultadoZona.perdedor.idEquipo =
            lsPartidos[i].local.idEquipo;
        }

        if (this.idFase == 3 && lsPartidos[i].ganador.idEquipo) {
          if (
            lsPartidos[i].ganador.idEquipo ==
            lsPartidos[i].local.idEquipo
          ) {
            lsPartidos[i].resultadoZona.ganador.idEquipo =
              lsPartidos[i].local.idEquipo;
            lsPartidos[i].resultadoZona.perdedor.idEquipo =
              lsPartidos[i].visitante.idEquipo;
          } else {
            lsPartidos[i].resultadoZona.ganador.idEquipo =
              lsPartidos[i].visitante.idEquipo;
            lsPartidos[i].resultadoZona.perdedor.idEquipo =
              lsPartidos[i].local.idEquipo;
          }
          lsPartidos[i].resultadoZona.empate = null;
        }
      }
    }
    this.partidoService
      .create(lsPartidos, this.idFase, this.idTorneo, this.esInterzonal)
      .subscribe(
        data => {
          if (data) {
            this.toastr.success(
              'Se registraron correctamente los resultados.',
              'Éxito!'
            );
            this.limpiarCampos();
            this.spinnerService.hide();
          }
        },
        error => {
          this.toastr.error('Intente nuevamente más tarde.', 'Error!');
          this.spinnerService.hide();
        }
      );
    lsPartidos = [];
  }

  rojaLocal(partido: Partido) {}

  rojaVisistante(partido: Partido) {}

  openConfirmationDialogLocal(
    sancion,
    lsFechasInicio,
    lsFechasFin,
    lsTiposSancion,
    partido
  ) {
    const conjunto = Array<any>();
    conjunto.push(sancion);
    conjunto.push(lsFechasInicio);
    conjunto.push(lsFechasFin);
    conjunto.push(lsTiposSancion);
    conjunto.push(partido);
    this.dialogRef = this.dialog.open(SancionDialog, {
      data: conjunto,
      height: '45%',
      width: '65%',
      disableClose: false
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.rojaLocal(result.data);
      }
      this.dialogRef = null;
    });
  }

  openConfirmationDialogVisitante(
    sancion,
    lsFechasInicio,
    lsFechasFin,
    lsTiposSancion,
    partido
  ) {
    const conjunto = Array<any>();
    conjunto.push(sancion);
    conjunto.push(lsFechasInicio);
    conjunto.push(lsFechasFin);
    conjunto.push(lsTiposSancion);
    conjunto.push(partido);
    this.dialogRefV = this.dialog.open(SancionDialogV, {
      data: conjunto,
      height: '45%',
      width: '65%',
      disableClose: false
    });

    this.dialogRefV.afterClosed().subscribe(result => {
      if (result) {
        this.rojaVisistante(result.data);
      }
      this.dialogRefV = null;
    });
  }

  fechasPorZonaLocal(partido: Partido) {
    if (this.esInterzonal == 0) {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        dataF => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechas(this.zona.id_zona, this.idTorneo)
            .subscribe(
              data => {
                const sancion = new Sancion();
                this.fixture = data;
                /*this.lsFechasInicio = this.fixture.fechas;
                            this.lsFechasFin = this.fixture.fechas;*/
                sancion.equipo.idEquipo = partido.local[0].idEquipo;
                sancion.jugador = this.jugadorLocal;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogLocal(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              error => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        error => {}
      );
    } else {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        dataF => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechasInterzonales(this.idTorneo)
            .subscribe(
              data => {
                const sancion = new Sancion();
                /*this.lsFechasInicio = data;
                            this.lsFechasFin = data;*/
                sancion.equipo.idEquipo = partido.local[0].idEquipo;
                sancion.jugador = this.jugadorLocal;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogLocal(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              error => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        error => {}
      );
    }
  }

  amarillaLocal(partido: Partido) {
    const sancion = new Sancion();
    sancion.equipo.idEquipo = partido.local[0].idEquipo;
    sancion.jugador = this.jugadorLocal;
    sancion.partido.idPartido = partido.idPartido;
    sancion.zona.id_zona = this.zona != null ? this.zona.id_zona : null;
    sancion.tipo.id = 1;
    sancion.fechaInicio.fecha = this.fecha.fecha;
    sancion.fechaFin.fecha = this.fecha.fecha;
    sancion.tipo.descripcion = 'Amarilla';
    sancion.fase.idFase = this.idFase;
    partido.sanciones.push(sancion);
  }

  amarillaVisitante(partido: Partido) {
    const sancion = new Sancion();
    sancion.equipo.idEquipo = partido.visitante[0].idEquipo;
    sancion.jugador = this.jugadorVisitante;
    sancion.partido.idPartido = partido.idPartido;
    sancion.zona.id_zona = this.zona != null ? this.zona.id_zona : null;
    sancion.tipo.id = 1;
    sancion.tipo.descripcion = 'Amarilla';
    sancion.fechaInicio.fecha = this.fecha.fecha;
    sancion.fechaFin.fecha = this.fecha.fecha;
    sancion.fase.idFase = this.idFase;
    partido.sanciones.push(sancion);
  }

  public calcularFairPlay(sanciones: Array<Sancion>): number {
    let fair_play = 0;

    const rojas = sanciones.filter((p) => p.tipo.id === 2);
    const amarillas = sanciones.filter((p) => p.tipo.id === 1);

    if (!rojas && amarillas) {
      fair_play = amarillas.length;
    } else if (rojas && !amarillas) {
      fair_play = rojas.length * 3;
    } else if (rojas && amarillas) {
      fair_play = amarillas.length + (rojas.length * 3);
    }
    return -fair_play;
  }

  fechasPorZonaVisitante(partido: Partido) {
    if (this.esInterzonal == 0) {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        dataF => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechas(this.zona.id_zona, this.idTorneo)
            .subscribe(
              data => {
                const sancion = new Sancion();
                this.fixture = data;
                /* this.lsFechasInicio = this.fixture.fechas;
                             this.lsFechasFin = this.fixture.fechas;*/
                sancion.equipo.idEquipo = partido.visitante[0].idEquipo;
                sancion.jugador = this.jugadorVisitante;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogVisitante(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              error => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        error => {}
      );
    } else {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        dataF => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechasInterzonales(this.idTorneo)
            .subscribe(
              data => {
                const sancion = new Sancion();
                /* this.lsFechasInicio = data;
                             this.lsFechasFin = data;*/
                sancion.equipo.idEquipo = partido.visitante[0].idEquipo;
                sancion.jugador = this.jugadorVisitante;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogVisitante(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              error => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        error => {}
      );
    }
  }

  droppableItemClass = (item: any) => `${item.class} ${item.inputType}`;

  builderDrag(e: any) {
    const item = e.value;
    item.data =
      item.inputType === 'number'
        ? (Math.random() * 100) | 0
        : Math.random()
            .toString(36)
            .substring(20);
  }

  canMove(e: any): boolean {
    return e.indexOf('Disabled') === -1;
  }

  public compararHorariosBack(partido: Partido) {
    this.fixtureService.obtenerPartidosClub(partido).subscribe(
      data => {
        let partido = new IPartido();
        partido = data;
        if (partido.id_partido > 0) {
        }
      },
      error => {
        this.toastr.error('Intente nuevamente más tarde.', 'Error!');
      }
    );
  }

  public dibujarPartidos() {
    try {
      for (let j = this.partidos.length - 1; j >= 0; j--) {
        if (
          this.partidos[j].local.length == 0 &&
          this.partidos[j].visitante.length == 0
        ) {
          this.partidos.splice(j, 1);
        }
      }
    } catch (Exception) {}
    for (let i = 0; i < this.cantidadPartidos; i++) {
      this.partidos.push(new IPartido());
    }
  }

  enfrentamiento(obj: any) {}

  verificacionComponentes() {
    for (let i = 0; i < this.partidos.length; i++) {
      if (
        this.partidos[i].local.length == 0 ||
        this.partidos[i].visitante.length == 0 ||
        this.partidos[i].cancha.id_cancha == null ||
        this.partidos[i].horario.id_horario == null
      ) {
        this.check = false;
      } else {
        this.check = true;
      }
    }
    return this.check;
  }

  limpiarCampos() {
    this.partidos = [];
    this.cantidadPartidos = null;
    this.equipos = [];
    this.zona = null;
    this.ngOnInit();
  }

  limpiarComp() {}

  public onSelectPenales(value: boolean) {
    console.error('CHECK' + value);
  }

  routeAlta() {
    this.router.navigate(['home/resultado-carga']);
  }

  routeModificacion() {
    this.router.navigate(['home/resultado-update']);
  }
}
