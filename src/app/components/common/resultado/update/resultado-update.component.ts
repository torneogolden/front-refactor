import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MatSort,
  MatTableDataSource,
} from '@angular/material';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppConfig } from '../../../../app.config';
import {
  Cancha,
  Equipo,
  Fecha,
  Fixture,
  Gol,
  HorarioFijo,
  IPartido,
  Jugador,
  Partido,
  Sancion,
  TipoSancion,
  Zona,
} from '../../../../entities/index';
import { ParserService } from '../../../../services/common-services/index';
import { FileService } from '../../../../services/entity-services/file.service';
import {
  CanchaService,
  EquipoService,
  FixtureService,
  HorarioService,
  PartidoService,
  SancionService,
  ZonaService,
} from '../../../../services/entity-services/index';
import { ConfirmationDialog } from '../../../common/dialog/index';
import { SancionDialog, SancionDialogV } from '.././index';

@Component({
  selector: 'resultado-update',
  moduleId: module.id,
  templateUrl: './resultado-update.component.html',
  styleUrls: ['./resultado-update.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [EquipoService, ZonaService],
})
export class ResultadoUpdateComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  dialogRef: MatDialogRef<SancionDialog>;
  dialogRefV: MatDialogRef<SancionDialogV>;
  dialogRefBorrado: MatDialogRef<ConfirmationDialog>;
  displayedColumns = ['apellido', 'nombre', 'id_sancion'];
  displayedColumnsG = ['apellido', 'gol', 'id_gol'];

  lsTiposSanciones = new Array<TipoSancion>();
  lsCanchas = new Array<Cancha>();
  horarios = new Array<HorarioFijo>();
  lsZonas = new Array<Zona>();
  lsEquipos = new Array<Equipo>();
  idTorneo: number;
  idFase: number;
  fecha: Fecha = new Fecha();
  zona: Zona = new Zona();
  equipo: Equipo = new Equipo();
  partido: Partido;
  jugadorVisitante = new Jugador();
  jugadorLocal = new Jugador();
  fixture = new Fixture();
  lsFechasInicio = new Array<Fecha>();
  lsFechasFin = new Array<Fecha>();
  golesABorrar = new Array<Gol>();
  partidos = new Array<IPartido>();
  dataSourceSLocal = new MatTableDataSource<Sancion>();
  dataSourceSVisitante = new MatTableDataSource<Sancion>();
  dataSourceGLocal = new MatTableDataSource<Gol>();
  dataSourceGVisitante = new MatTableDataSource<Gol>();
  esInterzonal = 0;
  checked: boolean;
  evento: boolean;

  constructor(
    private fileService: FileService,
    public equipoService: EquipoService,
    private router: Router,
    public zonaService: ZonaService,
    public toastr: ToastsManager,
    public horarioService: HorarioService,
    public canchaService: CanchaService,
    public parserService: ParserService,
    public fixtureService: FixtureService,
    public dialog: MatDialog,
    public sancionService: SancionService,
    public partidoService: PartidoService,
    public config: AppConfig,
    private spinnerService: Ng4LoadingSpinnerService
  ) {
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
    this.idFase = Number(sessionStorage.getItem('fase'));
  }
  ngOnInit() {
    this.zonaService.getAll(this.idTorneo).subscribe(
      (data) => {
        this.lsZonas = [];
        for (let i = 0; i < data.length; i++) {
          let zona: Zona;
          zona = data[i];
          if (zona.torneo.idTorneo != null) {
            this.lsZonas.push(zona);
          }
        }
      },
      (error) => {}
    );

    this.sancionService.getAll().subscribe(
      (data) => {
        this.lsTiposSanciones = [];
        for (let i = 0; i < data.length; i++) {
          let tipo: TipoSancion;
          tipo = data[i];
          if (tipo.id != 1) {
            this.lsTiposSanciones.push(tipo);
          }
        }
      },
      (error) => {}
    );

    this.horarioService.getAll().subscribe(
      (data) => {
        this.horarios = [];
        for (let i = 0; i < data.length; i++) {
          let horario = new HorarioFijo();
          horario = data[i];
          this.horarios.push(horario);
        }
      },
      (error) => {
        error.json()['Message'];
      }
    );

    this.canchaService.getAll().subscribe(
      (data) => {
        this.lsCanchas = [];
        for (let i = 0; i < data.length; i++) {
          let cancha = new Cancha();
          cancha = data[i];
          this.lsCanchas.push(cancha);
        }
      },
      (error) => {
        error.json()['Message'];
      }
    );
  }

  checkValue(event: any) {
    this.evento = event;
    if (event) {
      this.esInterzonal = 1;
      this.zona = null;
      this.equipo = null;
      this.obtenerEquiposTodos();
    } else {
      this.esInterzonal = 0;
      this.zona = null;
      this.equipo = null;
    }
    this.obtenerPartido();
  }

  obtenerEquipos(zona: Zona) {
    if (!zona.id_zona) {
      this.lsEquipos = [];
    }
    this.equipoService.getAllPorZona(zona.id_zona).subscribe(
      (data) => {
        this.lsEquipos = [];
        for (let i = 0; i < data.length; i++) {
          let equipo = new Equipo();
          equipo = data[i];
          this.lsEquipos.push(equipo);
        }
      },
      (error) => {
        error.json()['Message'];
      }
    );
  }

  obtenerEquiposTodos() {
    return this.equipoService.getAllPorTorneo(this.idTorneo).subscribe(
      (data) => {
        this.lsEquipos = [];
        for (let i = 0; i < data.length; i++) {
          let equipo = new Equipo();
          equipo = data[i];
          this.lsEquipos.push(equipo);
        }
      },
      (error) => {
        error.json()['Message'];
      }
    );
  }

  obtenerPartido() {
    this.fixtureService
      .obtenerPartidoFZEquipo(
        this.fecha,
        this.equipo == null ? 0 : this.equipo.idEquipo,
        this.idTorneo,
        this.zona == null ? 0 : this.zona.id_zona,
        this.esInterzonal
      )
      .subscribe(
        (data) => {
          if (data) {
            this.partido = data;
            this.dataSourceSLocal = new MatTableDataSource(
              this.partido.sanciones
            );
            this.dataSourceSVisitante = new MatTableDataSource(
              this.partido.sanciones
            );
            this.dataSourceGLocal = new MatTableDataSource(
              this.partido.golesLocal
            );
            this.dataSourceGVisitante = new MatTableDataSource(
              this.partido.golesVisitante
            );
            // this.partido.desImagenes = true;
            //  this.partido.desImagenesV = true;
            //   this.partido.jugadorVisitante = new Jugador();
            // this.partido.jugadorLocal = new Jugador();
            // this.partido.jugadorVisitante.acumAmarillas = 0;
            //  this.partido.jugadorVisitante.acumRojas = 0;
          }
        },
        (error) => {
          error.json()['Message'];
          this.partido = null;
        }
      );
  }

  amarillaLocal(partido: Partido) {
    const sancion = new Sancion();
    sancion.equipo.idEquipo = partido.local[0].idEquipo;
    sancion.jugador = this.jugadorLocal;
    sancion.partido.idPartido = partido.idPartido;
    sancion.zona.id_zona = this.zona != null ? this.zona.id_zona : null;
    sancion.tipo.id = 1;
    sancion.fechaInicio.fecha = this.fecha.fecha;
    sancion.fechaFin.fecha = this.fecha.fecha;
    sancion.tipo.descripcion = 'Amarilla';
    sancion.fase.idFase = this.idFase;
    this.partido.sanciones.push(sancion);
    this.dataSourceSLocal = new MatTableDataSource(
      this.partido.sanciones
    );
  }

  amarillaVisitante(partido: Partido) {
    const sancion = new Sancion();
    sancion.equipo.idEquipo = partido.visitante[0].idEquipo;
    sancion.jugador = this.jugadorVisitante;
    sancion.partido.idPartido = partido.idPartido;
    sancion.zona.id_zona = this.zona != null ? this.zona.id_zona : null;
    sancion.tipo.id = 1;
    sancion.tipo.descripcion = 'Amarilla';
    sancion.fechaInicio.fecha = this.fecha.fecha;
    sancion.fechaFin.fecha = this.fecha.fecha;
    sancion.fase.idFase = this.idFase;
    this.partido.sanciones.push(sancion);
    this.dataSourceSVisitante = new MatTableDataSource(
      this.partido.sanciones
    );
  }

  onChangeLocal(jugador: Jugador, partido: Partido) {
    if (jugador.apellido != null) {
   //   partido.desImagenes = false;
    } else {
     // partido.desImagenes = true;
     // this.partido.jugadorLocal.acumAmarillas = 0;
     // this.partido.jugadorLocal.acumRojas = 0;
    }

    this.sancionService
      .getAcumuladoTarjetas(this.idTorneo, jugador.idJugador)
      .subscribe(
        (data) => {
          let sanciones = new Array<Sancion>();
          sanciones = data;
          this.calcularRojasAmarillasL(jugador, sanciones);
        },
        (error) => {}
      );
  }
  onChangeVisitante(jugador: Jugador, partido: Partido) {
    if (jugador.apellido != null) {
   //   partido.desImagenesV = false;
    } else {
    //  partido.desImagenesV = true;
    //  this.partido.jugadorVisitante.acumAmarillas = 0;
    //  this.partido.jugadorVisitante.acumRojas = 0;
    }

    this.sancionService
      .getAcumuladoTarjetas(this.idTorneo, jugador.idJugador)
      .subscribe(
        (data) => {
          let sanciones = new Array<Sancion>();
          sanciones = data;
          this.calcularRojasAmarillasV(jugador, sanciones);
        },
        (error) => {}
      );
  }

  calcularRojasAmarillasL(jugador: Jugador, sanciones: Array<Sancion>) {
    let amarillas = 0;
    let rojas = 0;
    for (let j = 0; j < this.partidos.length; j++) {
      this.partidos[j].jugadorLocal = new Jugador();
    }
    for (let i = 0; i < sanciones.length; i++) {
      if (sanciones[i].tipo.id == 1) {
        //Acumulo Amarillas
        amarillas = amarillas + 1;
      }
      if (sanciones[i].tipo.id == 2) {
        //Acumulo Rojas - No distingo el resto
        rojas = rojas + 1;
      }
    }

    while (amarillas > 4) {
      amarillas = amarillas - 5;
    }
    jugador.acumAmarillas = amarillas;
    jugador.acumRojas = rojas;
   // this.partido.jugadorLocal = jugador;
  }

  calcularRojasAmarillasV(jugador: Jugador, sanciones: Array<Sancion>) {
    let amarillas = 0;
    let rojas = 0;
    for (let i = 0; i < sanciones.length; i++) {
      if (sanciones[i].tipo.id == 1) {
        //Acumulo Amarillas
        amarillas = amarillas + 1;
      }
      if (sanciones[i].tipo.id == 2) {
        //Acumulo Rojas - No distingo el resto
        rojas = rojas + 1;
      }
    }
    jugador.acumAmarillas = amarillas;
    jugador.acumRojas = rojas;
    //this.partido.jugadorVisitante = jugador;
  }

  golLocal(partido: Partido) {
    const gol = new Gol();
    gol.equipo.idEquipo = partido.local[0].idEquipo;
    gol.jugador = this.jugadorLocal;
    gol.partido.idPartido = partido.idPartido;
    this.partido.golesLocal.push(gol);
    this.dataSourceGLocal = new MatTableDataSource(this.partido.golesLocal);
  }

  golVisitante(partido: Partido) {
    const gol = new Gol();
    gol.equipo.idEquipo = partido.visitante[0].idEquipo;
    gol.jugador = this.jugadorVisitante;
    gol.partido.idPartido = partido.idPartido;
    // this.golesVisitante.push(gol);
    this.partido.golesVisitante.push(gol);
    this.dataSourceGVisitante = new MatTableDataSource(
      this.partido.golesVisitante
    );
  }

  fechasPorZonaLocal(partido: Partido) {
    if (this.esInterzonal == 0) {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        (dataF) => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechas(this.zona.id_zona, this.idTorneo)
            .subscribe(
              (data) => {
                const sancion = new Sancion();
                this.fixture = data;
                /* this.lsFechasInicio = this.fixture.fechas;
                             this.lsFechasFin = this.fixture.fechas;*/
                sancion.equipo.idEquipo = partido.local[0].idEquipo;
                sancion.jugador = this.jugadorLocal;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogLocal(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              (error) => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        (error) => {}
      );
    } else {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        (dataF) => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechasInterzonales(this.idTorneo)
            .subscribe(
              (data) => {
                const sancion = new Sancion();
                /* this.lsFechasInicio = data;
                             this.lsFechasFin = data;*/
                sancion.equipo.idEquipo = partido.local[0].idEquipo;
                sancion.jugador = this.jugadorLocal;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogLocal(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              (error) => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        (error) => {}
      );
    }
  }

  fechasPorZonaVisitante(partido: Partido) {
    if (this.esInterzonal == 0) {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        (dataF) => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechas(this.zona.id_zona, this.idTorneo)
            .subscribe(
              (data) => {
                const sancion = new Sancion();

                this.fixture = data;
                /*this.lsFechasInicio = this.fixture.fechas;
                            this.lsFechasFin = this.fixture.fechas;*/
                sancion.equipo.idEquipo = partido.visitante[0].idEquipo;
                sancion.jugador = this.jugadorVisitante;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogVisitante(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              (error) => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        (error) => {}
      );
    } else {
      this.fixtureService.obtenerFechasParaSanciones(this.idTorneo).subscribe(
        (dataF) => {
          this.lsFechasFin = [];
          this.lsFechasInicio = [];
          this.lsFechasInicio = dataF.fechas;
          this.lsFechasFin = dataF.fechas;
          this.fixtureService
            .obtenerFechasInterzonales(this.idTorneo)
            .subscribe(
              (data) => {
                const sancion = new Sancion();
                /*this.lsFechasInicio = data;
                            this.lsFechasFin = data;*/
                sancion.equipo.idEquipo = partido.visitante[0].idEquipo;
                sancion.jugador = this.jugadorVisitante;
                sancion.partido.idPartido = partido.idPartido;
                sancion.zona.id_zona =
                  this.zona != null ? this.zona.id_zona : null;
                sancion.fase.idFase = this.idFase;

                this.openConfirmationDialogVisitante(
                  sancion,
                  this.lsFechasInicio,
                  this.lsFechasFin,
                  this.lsTiposSanciones,
                  partido
                );
              },
              (error) => {
                this.lsFechasFin = [];
                this.lsFechasInicio = [];
              }
            );
        },
        (error) => {}
      );
    }
  }

  openConfirmationDialogLocal(
    sancion,
    lsFechasInicio,
    lsFechasFin,
    lsTiposSancion,
    partido
  ) {
    const conjunto = Array<any>();
    conjunto.push(sancion);
    conjunto.push(lsFechasInicio);
    conjunto.push(lsFechasFin);
    conjunto.push(lsTiposSancion);
    conjunto.push(partido);
    this.dialogRef = this.dialog.open(SancionDialog, {
      data: conjunto,
      height: '45%',
      width: '65%',
      disableClose: false,
    });

    this.dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.dataSourceSLocal = new MatTableDataSource(
          this.partido.sanciones
        );
      }
      this.dialogRef = null;
    });
  }

  openConfirmationDialogVisitante(
    sancion,
    lsFechasInicio,
    lsFechasFin,
    lsTiposSancion,
    partido
  ) {
    const conjunto = Array<any>();
    conjunto.push(sancion);
    conjunto.push(lsFechasInicio);
    conjunto.push(lsFechasFin);
    conjunto.push(lsTiposSancion);
    conjunto.push(partido);
    this.dialogRefV = this.dialog.open(SancionDialogV, {
      data: conjunto,
      height: '45%',
      width: '65%',
      disableClose: false,
    });

    this.dialogRefV.afterClosed().subscribe((result) => {
      if (result) {
        this.dataSourceSVisitante = new MatTableDataSource(
          this.partido.sanciones
        );
      }
      this.dialogRefV = null;
    });
  }

  eliminarSancionLocal(id_sancion: number, idJugador: number) {
    this.dialogRefBorrado = this.dialog.open(ConfirmationDialog, {
      height: '200px',
      width: '350px',
      disableClose: false,
    });
    this.dialogRefBorrado.componentInstance.confirmMessage =
      'Se eliminara la sanción al jugador.';

    this.dialogRefBorrado.afterClosed().subscribe((result) => {
      if (result) {
        for (let i = 0; i < this.partido.sanciones.length; i++) {
          if (
            idJugador ==
            this.partido.sanciones[i]['jugador']['idJugador']
          ) {
            this.partido.sanciones.splice(i, 1);
            this.dataSourceSLocal = new MatTableDataSource(
              this.partido.sanciones
            );
            break;
          }
        }

        if (id_sancion) {
          this.partidoService.borrarSancion(id_sancion).subscribe(
            (data) => {
              this.toastr.success(
                'La sanción fue eliminada correctamente.',
                'Éxito!'
              );
            },
            (error) => {}
          );
        }
      }
      this.dialogRefBorrado = null;
    });
  }

  eliminarSancionVisitante(id_sancion: number, idJugador: number) {
    this.dialogRefBorrado = this.dialog.open(ConfirmationDialog, {
      height: '200px',
      width: '350px',
      disableClose: false,
    });
    this.dialogRefBorrado.componentInstance.confirmMessage =
      'Se eliminara la sanción al jugador.';

    this.dialogRefBorrado.afterClosed().subscribe((result) => {
      if (result) {
        for (let i = 0; i < this.partido.sanciones.length; i++) {
          if (
            idJugador ==
            this.partido.sanciones[i]['jugador']['idJugador']
          ) {
            this.partido.sanciones.splice(i, 1);
            this.dataSourceSVisitante = new MatTableDataSource(
              this.partido.sanciones
            );
            break;
          }
        }
        if (id_sancion) {
          this.partidoService.borrarSancion(id_sancion).subscribe(
            (data) => {
              this.toastr.success(
                'La sanción fue eliminada correctamente.',
                'Éxito!'
              );
            },
            (error) => {}
          );
        }
      }
      this.dialogRefBorrado = null;
    });
  }

  eliminarGolLocal(id_gol: number, idJugador: number) {
    this.dialogRefBorrado = this.dialog.open(ConfirmationDialog, {
      height: '200px',
      width: '350px',
      disableClose: false,
    });
    this.dialogRefBorrado.componentInstance.confirmMessage =
      'Se eliminara el gol al jugador.';

    this.dialogRefBorrado.afterClosed().subscribe((result) => {
      if (result) {
        for (let i = 0; i < this.partido.golesLocal.length; i++) {
          if (
            idJugador == this.partido.golesLocal[i]['jugador']['idJugador']
          ) {
            if (id_gol) {
              const gol = new Gol();
              const equipo = new Equipo();
              gol.jugador.idJugador =
                this.partido.golesLocal[i]['jugador']['idJugador'];
              gol.id_gol = this.partido.golesLocal[i]['id_gol'];
              gol.equipo = equipo;
              gol.equipo.idEquipo = this.partido['local'][0]['idEquipo'];
              this.golesABorrar.push(gol);
            }
            this.partido.golesLocal.splice(i, 1);
            this.dataSourceGLocal = new MatTableDataSource(
              this.partido.golesLocal
            );
            break;
          }
        }

        /*  if (id_gol) {
                      this.partidoService.borrarGol(id_gol, this.idFase, this.zona == null ? 0 : this.zona.id_zona).subscribe(
                          data => {
                              this.toastr.success("El gol fue eliminado correctamente.", "Éxito!");
                          }, error => {

                          }
                      );
                  }*/
      }
      this.dialogRefBorrado = null;
    });
  }

  eliminarGolVisitante(id_gol: number, idJugador: number) {
    this.dialogRefBorrado = this.dialog.open(ConfirmationDialog, {
      height: '200px',
      width: '350px',
      disableClose: false,
    });
    this.dialogRefBorrado.componentInstance.confirmMessage =
      'Se eliminara el gol al jugador.';

    this.dialogRefBorrado.afterClosed().subscribe((result) => {
      if (result) {
        for (let i = 0; i < this.partido.golesVisitante.length; i++) {
          if (
            idJugador ==
            this.partido.golesVisitante[i]['jugador']['idJugador']
          ) {
            if (id_gol) {
              const gol = new Gol();
              const equipo = new Equipo();
              gol.jugador.idJugador =
                this.partido.golesVisitante[i]['jugador']['idJugador'];
              gol.id_gol = this.partido.golesVisitante[i]['id_gol'];
              gol.equipo = equipo;
              gol.equipo.idEquipo = this.partido['visitante'][0]['idEquipo'];
              this.golesABorrar.push(gol);
            }
            this.partido.golesVisitante.splice(i, 1);
            this.dataSourceGVisitante = new MatTableDataSource(
              this.partido.golesVisitante
            );
            break;
          }
        }

        /*  if (id_gol) {
                      this.partidoService.borrarGol(id_gol, this.idFase, this.zona == null ? 0 : this.zona.id_zona).subscribe(
                          data => {
                              this.toastr.success("El gol fue eliminado correctamente.", "Éxito!");
                          }, error => {

                          }
                      );
                  }*/
      }
      this.dialogRefBorrado = null;
    });
  }

  modificarResultado() {
    this.spinnerService.show();
    this.partido.golesABorrar = this.golesABorrar;
    let partido = new Partido();
    if (this.partido) {
      partido = this.parserService.parseResultado(this.partido, this.idTorneo);
    }

    partido.local.fair_play = this.calcularFairPlay(partido.sanciones);
    partido.visitante.fair_play = this.calcularFairPlay(
      partido.sanciones
    );

    if (this.esInterzonal == 1) {
      if (partido.golesLocal.length == partido.golesVisitante.length) {
        partido.resultado.ganador.idEquipo = partido.local.idEquipo;
        partido.resultado.perdedor.idEquipo = partido.visitante.idEquipo;
        partido.resultado.empate = 1;
      } else if (partido.golesLocal.length > partido.golesVisitante.length) {
        partido.resultado.ganador.idEquipo = partido.local.idEquipo;
        partido.resultado.perdedor.idEquipo = partido.visitante.idEquipo;
      } else {
        partido.resultado.ganador.idEquipo = partido.visitante.idEquipo;
        partido.resultado.perdedor.idEquipo = partido.local.idEquipo;
      }
    } else {
      partido.resultadoZona.zona = this.zona;
      if (partido.golesLocal.length == partido.golesVisitante.length) {
        partido.resultadoZona.ganador.idEquipo = partido.local.idEquipo;
        partido.resultadoZona.perdedor.idEquipo = partido.visitante.idEquipo;
        partido.resultadoZona.empate = 1;
      } else if (partido.golesLocal.length > partido.golesVisitante.length) {
        partido.resultadoZona.ganador.idEquipo = partido.local.idEquipo;
        partido.resultadoZona.perdedor.idEquipo = partido.visitante.idEquipo;
      } else {
        partido.resultadoZona.ganador.idEquipo = partido.visitante.idEquipo;
        partido.resultadoZona.perdedor.idEquipo = partido.local.idEquipo;
      }
    }

    this.partidoService
      .update(partido, this.idFase, this.idTorneo, this.esInterzonal)
      .subscribe(
        (data) => {
          if (data) {
            this.toastr.success(
              'Se modificaron correctamente los resultados.',
              'Éxito!'
            );
            this.limpiarCampos();
            this.spinnerService.hide();
          }
        },
        (error) => {
          this.toastr.error('Intente nuevamente más tarde.', 'Error!');
          this.spinnerService.hide();
        }
      );
  }

  limpiarCampos() {
    this.partido = null;
    this.zona = null;
    this.equipo = null;
    this.golesABorrar = [];
    this.ngOnInit();
  }

  public calcularFairPlay(sanciones: Array<Sancion>): number {
    let fair_play = 0;

    const rojas = sanciones.filter(
      (p) => p.tipo.id === 2 && !p.tipo.id
    );
    const amarillas = sanciones.filter(
      (p) => p.tipo.id === 1 && !p.tipo.id
    );

    if (!rojas && amarillas) {
      fair_play = amarillas.length;
    } else if (rojas && !amarillas) {
      fair_play = rojas.length * 3;
    } else if (rojas && amarillas) {
      fair_play = amarillas.length + rojas.length * 3;
    }
    return -fair_play;
  }

  routeAlta() {
    this.router.navigate(['home/resultado-carga']);
  }

  routeModificacion() {
    this.router.navigate(['home/resultado-update']);
  }
}
