import { Component, Directive, ViewChild, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { Fecha, IPartido, Partido, Usuario } from '../../../../entities/index';
import {
  EquipoService,
  FixtureService,
} from '../../../../services/entity-services/index';
import { FileService } from '../../../../services/entity-services/file.service';
import { AppConfig } from '../../../../app.config';
import { FileType } from '../../../../services/FileType';

@Component({
  selector: 'resultado-visualizacion',
  moduleId: module.id,
  templateUrl: './resultado-visualizacion.component.html',
  styleUrls: ['./resultado-visualizacion.component.scss'],
  providers: [],
})
export class ResultadoisualizacionComponent implements OnInit {
  lsFechas = new Array<Fecha>();
  lsPartidos: Partido[];
  idTorneo: number;
  fecha = new Fecha();
  diaVisual: any;
  numeroFecha: number;
  fechaSeleccionada = false;
  user: Usuario;
  esAdmin = false;

  constructor(
    private equipoService: EquipoService,
    private fileService: FileService,
    private fixtureService: FixtureService,
    public config: AppConfig
  ) {
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
  }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (this.user != null) {
      if (this.user.perfil.id_perfil == 1) {
        this.esAdmin = true;
      }
    }

    this.fixtureService.obtenerFechasJugadas(this.idTorneo).subscribe(
      (data) => {
        if (data) {
          this.lsFechas = data;
          if (this.lsFechas.length) {
            this.mostrarResultadosFecha(
              this.lsFechas[this.lsFechas.length - 1],
              this.lsFechas.length - 1
            );
          }
        }
      },
      (error) => {
        this.lsFechas = [];
      }
    );
  }

  mostrarResultadosFecha(fecha: Fecha, i: number) {
    this.fixtureService.obtenerResultadosFecha(fecha, this.idTorneo).subscribe(
      (data) => {
        if (data) {
          data.filter((x) => {
            x.golesLocal =
              x.goles && x.goles.length
                ? x.goles
                    .filter((y) => y.idEquipo === x.local.idEquipo)
                    .map((y) => (y = { ...y, jugador: y.jugador.persona }))
                : [];
            x.sancionesLocal =
              x.sanciones && x.sanciones.length
                ? x.sanciones
                    .filter((y) => y.idEquipo === x.local.idEquipo)
                    .map((y) => (y = { ...y, jugador: y.jugador.persona }))
                : [];
            x.local.files = x.local.files.filter(
              (y) => y.idTipoArchivo === FileType.CAMISETA_ESCUDO
            );
            x.golesVisitante =
              x.goles && x.goles.length
                ? x.goles
                    .filter((y) => y.idEquipo === x.visitante.idEquipo)
                    .map((y) => (y = { ...y, jugador: y.jugador.persona }))
                : [];
            x.sancionesVisitante =
              x.sanciones && x.sanciones.length
                ? x.sanciones
                    .filter((y) => y.idEquipo === x.visitante.idEquipo)
                    .map((y) => (y = { ...y, jugador: y.jugador.persona }))
                : [];
            x.visitante.files = x.visitante.files.filter(
              (y) => y.idTipoArchivo === FileType.CAMISETA_ESCUDO
            );
          });
          this.lsPartidos = data;
          this.diaVisual = this.formatearFecha(new Date(fecha.fecha));
          this.numeroFecha = i + 1;
          this.fechaSeleccionada = true;
        }
      },
      (error) => {
        error.json()['Message'];
      }
    );
  }

  formatearFecha(fecha: Date) {
    const semana = new Array(
      'Domingo',
      'Lunes',
      'Martes',
      'Miércoles',
      'Jueves',
      'Viernes',
      'Sábado'
    );
    const dia_nombre = semana[fecha.getUTCDay()];
    const dd = fecha.getUTCDate();
    const mm = fecha.getMonth() + 1; //enero es 0

    /*         if (dd < 10) { var dd = '0' + dd }
                if (mm < 10) { var mm = '0' + mm } */

    return dia_nombre + ' ' + dd + '.' + mm;
  }
}
