import { Component, OnInit, ViewChild, ViewContainerRef, Input, Output, EventEmitter, HostListener, ViewEncapsulation } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { FileService } from '../../../services/entity-services/file.service';
import { Torneo, TipoTorneo, Modalidad, Regla, Categoria, Equipo, Zona, IEquipo } from '../../../entities/index';
import { EquipoService, ZonaService } from '../../../services/entity-services/index';
import { ToastsManager, Toast, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { AppConfig } from '../../../app.config';

@Component({
    selector: 'zona',
    moduleId: module.id,
    templateUrl: './zona.component.html',
    styleUrls: ['./zona.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [EquipoService, ZonaService]
})
export class ZonaComponent implements OnInit {

    equipos = new Array<IEquipo>();
    zonas = new Array<Zona>();
    lsEquiposA = new Array<Equipo>();
    lsEquiposB = new Array<Equipo>();
    lsEquiposC = new Array<Equipo>();
    lsEquiposD = new Array<Equipo>();
    lsEquiposE = new Array<Equipo>();
    lsEquiposF = new Array<Equipo>();
    lsEquiposG = new Array<Equipo>();
    lsEquiposH = new Array<Equipo>();

    imagesEscudos: Array<any> = [];
    cantidadZonas: number;
    idTorneo: number;
    idFase: number;
    btnHide = false;

    sourceItems = [
    ];
    constructor(private fileService: FileService,
        public equipoService: EquipoService,
        private router: Router,
        public zonaService: ZonaService,
        public toastr: ToastsManager,
        public config: AppConfig) {
        this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
        this.idFase = Number(sessionStorage.getItem('fase'));
    }

    ngOnInit() {
        this.equipoService.getAllSinZona(this.idTorneo).subscribe(
            data => {
                this.equipos = [];
                for (let j = 0; j < data.length; j++) {
                    const equipo = new IEquipo();
                    if (this.idTorneo == data[j]['torneo']['idTorneo']) {
                        equipo.idEquipo = data[j]['idEquipo'];
                        equipo.nombre = data[j]['nombre'];
                        equipo.logo = data[j]['logo'];
                        this.equipos.push(equipo);
                    }
                }
                for (let i = 0; i < this.equipos.length; i++) {
                    this.fileService.getImagesByEquipo(this.equipos[i].logo).subscribe(
                        data => {
                            if (data['ThumbPath'] != null) {
                                this.equipos[i].imagePath = data['ThumbPath'];
                            }
                        },
                        error => {
                        });
                }
            },
            error => {
                error.json()['Message'];
            });
    }

    zonaA: any[] = [];
    zonaB: any[] = [];
    zonaC: any[] = [];
    zonaD: any[] = [];
    zonaE: any[] = [];
    zonaF: any[] = [];
    zonaG: any[] = [];
    zonaH: any[] = [];

    droppableItemClass = (item: any) => `${item.class} ${item.inputType}`;

    builderDrag(e: any) {
        const item = e.value;
        item.data = item.inputType === 'number' ?
            (Math.random() * 100) | 0 :
            Math.random().toString(36).substring(20);
    }


    canMove(e: any): boolean {
        return e.indexOf('Disabled') === -1;
    }

    zona(equipos: any) {
    }

    armarZonas() {
        this.zonas = [];
        for (let i = 0; i < this.cantidadZonas; i++) {
            const zona = new Zona();
            if (this.idFase == 3) {
                zona.descripcion = 'Playoff ' + this.intercambioLetraPorNumero((i + 1).toString());
            } else {
                zona.descripcion = this.intercambioLetraPorNumero((i + 1).toString());
            }
            zona.torneo.idTorneo = this.idTorneo;
            zona.torneo.fase.idFase = this.idFase;
            this.zonas.push(zona);
        }
    }

    comprobacionZonasVacias() {
        let contador = 0;

        if (this.zonaA.length > 0) {
            contador = contador + 1;
        }

        if (this.zonaB.length > 0) {
            contador = contador + 1;
        }

        if (this.zonaC.length > 0) {
            contador = contador + 1;
        }

        if (this.zonaD.length > 0) {
            contador = contador + 1;
        }

        if (this.zonaE.length > 0) {
            contador = contador + 1;
        }

        if (this.zonaF.length > 0) {
            contador = contador + 1;
        }

        if (this.zonaG.length > 0) {
            contador = contador + 1;
        }
        if (this.zonaH.length > 0) {
            contador = contador + 1;
        }

        if (contador == this.cantidadZonas && this.idFase == 3) {
            this.altaZonaPlayoff();
        } else if (contador == this.cantidadZonas && this.idFase != 3) {
            this.altaZona();
        } else {
            this.toastr.error('Es necesario completar todas las zonas que eligió', 'Error!');

        }
    }

    altaZonaPlayoff() {
        for (let j = 0; j < this.zonas.length; j++) {
            switch (this.zonas[j].descripcion) {
                case 'Playoff A':
                    for (let i = 0; i < this.zonaA.length; i++) {
                        this.lsEquiposA.push(new Equipo(this.zonaA[i].idEquipo, this.zonaA[i].nombre));
                    }
                    this.zonas[j].equipos = this.lsEquiposA;
                    break;
                case 'Playoff B':
                    for (let i = 0; i < this.zonaB.length; i++) {
                        this.lsEquiposB.push(new Equipo(this.zonaB[i].idEquipo, this.zonaB[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposB;
                    break;
                case 'Playoff C':
                    for (let i = 0; i < this.zonaC.length; i++) {
                        this.lsEquiposC.push(new Equipo(this.zonaC[i].idEquipo, this.zonaC[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposC;
                    break;
                case 'Playoff D':
                    for (let i = 0; i < this.zonaD.length; i++) {
                        this.lsEquiposD.push(new Equipo(this.zonaD[i].idEquipo, this.zonaD[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposD;
                    break;
                case 'Playoff E':
                    for (let i = 0; i < this.zonaE.length; i++) {
                        this.lsEquiposE.push(new Equipo(this.zonaE[i].idEquipo, this.zonaE[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposE;
                    break;
                case 'Playoff F':
                    for (let i = 0; i < this.zonaF.length; i++) {
                        this.lsEquiposF.push(new Equipo(this.zonaF[i].idEquipo, this.zonaF[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposF;
                    break;
                case 'Playoff G':
                    for (let i = 0; i < this.zonaG.length; i++) {
                        this.lsEquiposG.push(new Equipo(this.zonaG[i].idEquipo, this.zonaG[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposG;
                    break;
                case 'Playoff H':
                    for (let i = 0; i < this.zonaH.length; i++) {
                        this.lsEquiposH.push(new Equipo(this.zonaH[i].idEquipo, this.zonaH[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposH;
                    break;
            }
        }

        this.zonaService.create(this.zonas).subscribe(
            data => {
                if (data) {
                    this.toastr.success('Las zonas se han creado', 'Éxito!');
                    this.limpiarCampos();
                }
            },
            error => {
                this.toastr.error('Hubo un problema para crear las zonas', 'Error!');
                this.limpiarCampos();
            });
    }
    altaZona() {
        for (let j = 0; j < this.zonas.length; j++) {
            switch (this.zonas[j].descripcion) {
                case 'A' || 'Playoff A':
                    for (let i = 0; i < this.zonaA.length; i++) {
                        this.lsEquiposA.push(new Equipo(this.zonaA[i].idEquipo, this.zonaA[i].nombre));
                    }
                    this.zonas[j].equipos = this.lsEquiposA;
                    break;
                case 'B' || 'Playoff B':
                    for (let i = 0; i < this.zonaB.length; i++) {
                        this.lsEquiposB.push(new Equipo(this.zonaB[i].idEquipo, this.zonaB[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposB;
                    break;
                case 'C' || 'Playoff C':
                    for (let i = 0; i < this.zonaC.length; i++) {
                        this.lsEquiposC.push(new Equipo(this.zonaC[i].idEquipo, this.zonaC[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposC;
                    break;
                case 'D' || 'Playoff D':
                    for (let i = 0; i < this.zonaD.length; i++) {
                        this.lsEquiposD.push(new Equipo(this.zonaD[i].idEquipo, this.zonaD[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposD;
                    break;
                case 'E' || 'Playoff E':
                    for (let i = 0; i < this.zonaE.length; i++) {
                        this.lsEquiposE.push(new Equipo(this.zonaE[i].idEquipo, this.zonaE[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposE;
                    break;
                case 'F' || 'Playoff F':
                    for (let i = 0; i < this.zonaF.length; i++) {
                        this.lsEquiposF.push(new Equipo(this.zonaF[i].idEquipo, this.zonaF[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposF;
                    break;
                case 'G' || 'Playoff G':
                    for (let i = 0; i < this.zonaG.length; i++) {
                        this.lsEquiposG.push(new Equipo(this.zonaG[i].idEquipo, this.zonaG[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposG;
                    break;
                case 'H' || 'Playoff H':
                    for (let i = 0; i < this.zonaH.length; i++) {
                        this.lsEquiposH.push(new Equipo(this.zonaH[i].idEquipo, this.zonaH[i].nombre));
                    } this.zonas[j].equipos = this.lsEquiposH;
                    break;
            }
        }

        this.zonaService.create(this.zonas).subscribe(
            data => {
                if (data) {
                    this.toastr.success('Las zonas se han creado', 'Éxito!');
                    this.limpiarCampos();
                }
            },
            error => {
                this.toastr.error('Hubo un problema para crear las zonas', 'Error!');
                this.limpiarCampos();
            });
    }
    limpiarCampos() {
        this.lsEquiposA = [];
        this.lsEquiposB = [];
        this.lsEquiposC = [];
        this.lsEquiposD = [];
        this.lsEquiposE = [];
        this.lsEquiposF = [];
        this.lsEquiposG = [];
        this.lsEquiposH = [];
        this.zonaA = [];
        this.zonaB = [];
        this.zonaC = [];
        this.zonaD = [];
        this.zonaE = [];
        this.zonaF = [];
        this.zonaG = [];
        this.zonaH = [];
        this.zonas = [];
        this.cantidadZonas = null;
        this.equipos = [];
        this.ngOnInit();
    }

    public intercambioLetraPorNumero(descripcion: string): string {
        let descripLetra: string;
        switch (descripcion) {
            case '1': {
                descripLetra = 'A';
                break;
            } case '2': {
                descripLetra = 'B';
                break;
            } case '3': {
                descripLetra = 'C';
                break;
            } case '4': {
                descripLetra = 'D';
                break;
            } case '5': {
                descripLetra = 'E';
                break;
            } case '6': {
                descripLetra = 'F';
                break;
            } case '7': {
                descripLetra = 'G';
                break;
            } case '8': {
                descripLetra = 'H';
                break;
            }
        }

        return descripLetra;
    }

    routeAlta() {
        this.router.navigate(['home/zona-carga']);
    }

    routeModificacion() {
        this.router.navigate(['home/zona-update']);
    }

    routeEliminar() {
        this.router.navigate(['home/zona-delete']);
    }
}
