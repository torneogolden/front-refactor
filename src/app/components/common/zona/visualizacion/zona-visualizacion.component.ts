import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AppConfig } from '../../../../app.config';
import { IEquipo, Zona } from '../../../../entities/index';
import {
  EquipoService,
  ZonaService,
} from '../../../../services/entity-services/index';
import { FileType } from '../../../../services/FileType';

@Component({
  selector: 'zona-visualizacion',
  moduleId: module.id,
  templateUrl: './zona-visualizacion.component.html',
  styleUrls: ['./zona-visualizacion.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [EquipoService, ZonaService],
})
export class ZonaVisualizacionComponent implements OnInit {
  equiposSinZona = new Array<IEquipo>();
  equipos = new Array<IEquipo>();
  esUltimoEquipo: Boolean = false;
  dibujarMensaje: Boolean = false;
  mensajePlayoffActivo: Boolean = false;
  zonas: Zona[] = [];

  idFase: number;
  imagesEscudos: Array<any> = [];
  cantidadZonas: number;
  idTorneo: number;

  sourceItems = [];
  constructor(
    public equipoService: EquipoService,
    public zonaService: ZonaService,
    public toastr: ToastsManager,
    public config: AppConfig
  ) {
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
    this.idFase = Number(sessionStorage.getItem('fase'));
  }

  ngOnInit() {
    if (this.idFase != 3) {
      this.zonaService.getAll(this.idTorneo).subscribe(
        (data) => {
          data.filter((x) =>
            x.equipos.forEach((y) => {
              y.files = y.files.filter(
                (z) => z.idTipoArchivo === FileType.CAMISETA_ESCUDO
              );
            })
          );
          this.zonas = data;
        },
        (error) => {}
      );
    } else {
      this.mensajePlayoffActivo = true;
    }
  }
  droppableItemClass = (item: any) => `${item.class} ${item.inputType}`;

  builderDrag(e: any) {
    const item = e.value;
    item.data =
      item.inputType === 'number'
        ? (Math.random() * 100) | 0
        : Math.random().toString(36).substring(20);
  }

  canMove(e: any): boolean {
    return e.indexOf('Disabled') === -1;
  }

  public zona(equipos: any): boolean {
    if (equipos.length == 1) {
      return false;
    }
    return true;
  }

  onchange(obj: any) {}
}
