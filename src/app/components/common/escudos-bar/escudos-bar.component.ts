import {
  Component,
  Directive,
  ViewChild,
  OnInit,
  ViewEncapsulation,
  Input,
} from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { LoginComponent } from '../../common/login/index';
import { Subscription } from 'rxjs/Subscription';
import { TorneoEmitter } from '../../../services/common-services/index';
import { FileService } from '../../../services/entity-services/file.service';
import {
  OnChanges,
  SimpleChanges,
} from '@angular/core/src/metadata/lifecycle_hooks';
import { EquipoService } from '../../../services/entity-services/index';
import { Equipo, IEquipo } from '../../../entities/index';
import { AppConfig } from '../../../app.config';
import { FileType } from '../../../services/FileType';

@Component({
  selector: 'escudos',
  moduleId: module.id,
  templateUrl: './escudos-bar.component.html',
  styleUrls: ['./escudos-bar.component.css'],
  providers: [],
  encapsulation: ViewEncapsulation.None,
})
export class EscudosComponent implements OnInit, OnChanges {
  @ViewChild(LoginComponent) login: LoginComponent;
  @Input() idTorneo = 0;
  nombre: String;
  images: Array<any> = [];

  lsEquipos: Equipo[];
  cantVisibles = 10;
  constructor(
    private torneoEmiiter: TorneoEmitter,
    private fileService: FileService,
    public equipoService: EquipoService,
    private router: Router,
    public config: AppConfig
  ) {}
  ngOnChanges(changes: SimpleChanges): void {
    this.initializeComponent();
  }
  ngOnInit() {
    this.initializeComponent();
  }

  initializeComponent() {
    this.nombre = sessionStorage.getItem('torneo');
    this.torneoEmiiter.onMyEvent.subscribe(
      (value: string) => (this.nombre = value)
    );
    this.equipoService.getAllPorTorneo(this.idTorneo).subscribe(
      (data) => {
        this.lsEquipos = data.filter(
          (x) =>
            (x.files = x.files.filter(
              (y) => y.idTipoArchivo === FileType.ESCUDO
            ))
        );
      },
      (error) => {
        error.json()['Message'];
      }
    );
  }

  verEquipo(idEquipo: number) {
    this.router.navigate(['home/equipo/' + idEquipo]);
  }
}
