import { Component, ViewChild, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { FooterComponent } from './footer/index';
import { NavComponent } from './nav/index';
import { HeaderComponent } from './header/index';
import { SectionComponent } from './section/index';
import { SharedService, TorneoService } from '../services/index';
import { Torneo } from '../entities/index';
import { TorneoEmitter } from '../services/common-services/index';
import { HomeComponent } from './home/index';

@Component({
  selector: 'container',
  moduleId: module.id,
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css'],
  providers: [TorneoService],
})
export class ContainerComponent implements OnInit {
  @ViewChild(FooterComponent) footer: FooterComponent;
  @ViewChild(NavComponent) nav: NavComponent;
  @ViewChild(SectionComponent) section: SectionComponent;
  @ViewChild(HeaderComponent) header: HeaderComponent;

  public lsTorneos = new Array<Torneo>();
  torneo: Torneo = new Torneo();
  idTorneo: number;
  subscription: any;

  constructor(
    private router: Router,
    private torneoService: TorneoService,
    private torneoEmitter: TorneoEmitter,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.setTorneo();
    this.subscription = this.sharedService
      .getNavChangeEmitter()
      .subscribe((id) => {
        this.idTorneo = id;
      });
  }

  setTorneo() {
    this.torneoService.getVigentes().subscribe(
      (data) => {
        for (let i = 0; i < data.length; i++) {
          const torneo = data[i];
          this.lsTorneos.push(torneo);
        }
        this.torneo = this.lsTorneos.find((x) => x.nombre.includes('GOLDEN'));
        sessionStorage.setItem('torneo', this.torneo.nombre);
        sessionStorage.setItem('idTorneo', String(this.torneo.idTorneo));
        sessionStorage.setItem('fase', String(this.torneo.fase.idFase));
        this.torneoEmitter.trigger(this.torneo.nombre);
        this.idTorneo = this.torneo.idTorneo;
      },
      (error) => {}
    );
  }
}
