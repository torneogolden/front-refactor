import { Component } from '@angular/core';
import {
  AfterViewInit,
  OnInit,
} from '@angular/core/src/metadata/lifecycle_hooks';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { AppConfig } from '../../app.config';
import { Noticia } from '../../entities/index';
import { SharedService } from '../../services';
import { NoticiaService } from '../../services/entity-services/index';
@Component({
  selector: 'home',
  moduleId: module.id,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [],
})
export class HomeComponent implements AfterViewInit, OnInit {
  public lsNoticiasPrincipales = [];
  public lsNoticiasSecundarias = [];
  public lsNoticiasHistoricas = [];

  url: string;
  noticiaPrincipal: Noticia;
  idTorneo: number;
  private subscription: Subscription;

  constructor(
    private noticiaService: NoticiaService,
    private router: Router,
    public config: AppConfig,
    private sharedService: SharedService
  ) {
    this.url = this.config.imgUrl;
    this.idTorneo = Number(sessionStorage.getItem('idTorneo'));
  }
  ngOnInit(): void {
    this.subscription = this.sharedService
      .getNavChangeEmitter()
      .subscribe((id) => {
        this.idTorneo = id;
        this.cargarNoticiasPrincipales();
      });
    this.cargarNoticiasPrincipales();
  }
  ngAfterViewInit() {
    (<any>window).twttr = (function (d, s, id) {
      let js: any,
        fjs = d.getElementsByTagName(s)[0],
        t = (<any>window).twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://platform.twitter.com/widgets.js';
      fjs.parentNode.insertBefore(js, fjs);

      t.e = [];
      t.ready = function (f: any) {
        t.e.push(f);
      };

      return t;
    })(document, 'script', 'twitter-wjs');

    if ((<any>window).twttr.ready()) (<any>window).twttr.widgets.load();

    this.subscription = this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        (<any>window).twttr = (function (d, s, id) {
          let js: any,
            fjs = d.getElementsByTagName(s)[0],
            t = (<any>window).twttr || {};
          if (d.getElementById(id)) return t;
          js = d.createElement(s);
          js.id = id;
          js.src = 'https://platform.twitter.com/widgets.js';
          fjs.parentNode.insertBefore(js, fjs);

          t.e = [];
          t.ready = function (f: any) {
            t.e.push(f);
          };

          return t;
        })(document, 'script', 'twitter-wjs');

        if ((<any>window).twttr.ready()) (<any>window).twttr.widgets.load();
      }
    });
  }

  verNoticia(idNoticia: number) {
    this.router.navigate(['home/noticia/' + idNoticia]);
  }

  cargarNoticiasPrincipales() {
    this.noticiaService.getPrincipales(this.idTorneo).subscribe(
      (data) => {
        this.noticiaPrincipal = data;
        this.cargarNoticiasSecundarias();
      },
      (error) => {
        this.lsNoticiasPrincipales = new Array<Noticia>();
        error.json()['Message'];
      }
    );
  }

  cargarNoticiasSecundarias() {
    this.noticiaService.getSecundarias(this.idTorneo).subscribe(
      (data) => {
        this.lsNoticiasSecundarias = data;
        this.cargarNoticiasHistoricas();
      },
      (error) => {
        this.lsNoticiasSecundarias = new Array<Noticia>();
        error.json()['Message'];
      }
    );
  }

  cargarNoticiasHistoricas() {
    this.noticiaService.getHistoricas(this.idTorneo).subscribe(
      (data) => {
        this.lsNoticiasHistoricas = data;
      },
      (error) => {
        this.lsNoticiasHistoricas = new Array<Noticia>();
        error.json()['Message'];
      }
    );
  }
}
