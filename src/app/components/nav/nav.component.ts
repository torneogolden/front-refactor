import {
  Component, OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { Torneo, Usuario } from '../../entities/index';
import {
  TorneoEmitter,
  TorneoLSEmitter
} from '../../services/common-services/index';
import { SharedService, TorneoService } from '../../services/index';
@Component({
  selector: 'menu',
  moduleId: module.id,
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  providers: [TorneoService],
})
export class NavComponent implements OnInit {
  torneo: Torneo = new Torneo();
  user: Usuario;
  public lsTorneos = new Array<Torneo>();
  idFase: number;
  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: SharedService,
    private torneoService: TorneoService,
    private torneoEmitter: TorneoEmitter,
    private torneoLsEmitter: TorneoLSEmitter,
    private sharedService: SharedService
  ) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));

    this.torneoLsEmitter.torneoUpdate.subscribe((value) => {
      this.lsTorneos.push(value);
    });
  }
  ngOnInit() {
    this.idFase = Number(sessionStorage.getItem('fase'));
    this.userService.newUserSubject.subscribe((data) => (this.user = data));
    this.subscription = this.sharedService
      .getPhaseChangeEmitter()
      .subscribe((id) => {
        this.idFase = id;
      });
    this.torneoService.getVigentes().subscribe(
      (data) => {
        this.lsTorneos = [];
        for (let i = 0; i < data.length; i++) {
          const torneo = data[i];
          this.lsTorneos.push(torneo);
        }
      },
      (error) => {}
    );
  }

  jugadoresCarga_Click() {
    this.router.navigate(['home/jugadores-carga']);
  }

  torneoCarga_Click() {
    this.router.navigate(['home/torneo-carga']);
  }

  equipoCarga_Click() {
    this.router.navigate(['home/equipo-carga']);
  }

  configuraciones_Click() {
    this.router.navigate(['home/configuraciones']);
  }

  noticiaCarga_Click() {
    this.router.navigate(['home/noticia-carga']);
  }

  noticias_Click() {
    this.router.navigate(['home/noticias']);
  }

  canchaCarga_Click() {
    this.router.navigate(['home/canchas']);
  }

  zonaCarga_Click() {
    this.router.navigate(['home/zona-carga']);
  }
  resultadoCarga_Click() {
    this.router.navigate(['home/resultado-carga']);
  }
  reglasCarga_click() {
    this.router.navigate(['home/reglas']);
  }
  equipos_Click() {
    this.router.navigate(['home/equipos']);
  }

  reglamento_Click() {
    this.router.navigate(['home/reglamento']);
  }

  posiciones_Click() {
    this.router.navigate(['home/posiciones']);
  }

  goleadores_Click() {
    this.router.navigate(['home/goleadores']);
  }

  usuariosCarga_Click() {
    this.router.navigate(['home/usuarios']);
  }

  fixture_Click() {
    this.router.navigate(['home/fixture']);
  }

  resultados_Click() {
    this.router.navigate(['home/resultados']);
  }

  zonasVisualizacion_Click() {
    this.router.navigate(['home/visualizacion-zonas']);
  }

  playoffVisualizacion_Click() {
    this.router.navigate(['home/playoff']);
  }

  fixtureAutomatico_Click() {
    this.router.navigate(['home/fixture-automatico']);
  }

  setTorneo(nombre: String, idTorneo: Number) {
    this.torneoService.getByName(nombre).subscribe(
      (data) => {
        this.torneo = data;
        sessionStorage.setItem('torneo', String(this.torneo.nombre));
        sessionStorage.setItem('idTorneo', String(this.torneo.idTorneo));
        sessionStorage.setItem('fase', String(this.torneo.fase.idFase));
        this.router.navigate(['home/noticias']);
        this.torneoEmitter.trigger(this.torneo.nombre);
        this.sharedService.emitNavChangeEvent(this.torneo.idTorneo);
        this.sharedService.emitPhaseChangeEvent(this.torneo.fase.idFase);
      },
      (error) => {}
    );
  }
}
